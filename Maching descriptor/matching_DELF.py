
import cv2
import time
import os
from absl import logging
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageOps
from scipy.spatial import cKDTree
from skimage.feature import plot_matches
from skimage.measure import ransac
from skimage.transform import AffineTransform
from six import BytesIO

import tensorflow as tf

import tensorflow_hub as hub
from six.moves.urllib.request import urlopen

class CNN(object):

    def __init__(self, model_filepath):

        # The file path of model
        self.model_filepath = model_filepath
        # Initialize the model
        self.load_graph(model_filepath = self.model_filepath)

    def load_graph(self, model_filepath):
        '''
        Lode trained model.
        '''
        print('Loading model...')
        self.graph = tf.Graph()
        self.sess = tf.InteractiveSession(graph = self.graph)

        with tf.gfile.GFile(model_filepath, 'rb') as f:
            graph_def = tf.GraphDef()
            graph_def.ParseFromString(f.read())

        print('Check out the input placeholders:')
        nodes = [n.name + ' => ' +  n.op for n in graph_def.node if n.op in ('Placeholder')]
        for node in nodes:
            print(node)

        # Define input tensor
        self.input = tf.placeholder(np.float32, shape = [None, 32, 32, 3], name='input')
        self.dropout_rate = tf.placeholder(tf.float32, shape = [], name = 'dropout_rate')

        tf.import_graph_def(graph_def, {'input': self.input, 'dropout_rate': self.dropout_rate})

        print('Model loading complete!')

        '''
        # Get layer names
        layers = [op.name for op in self.graph.get_operations()]
        for layer in layers:
            print(layer)
        '''

        '''
        # Check out the weights of the nodes
        weight_nodes = [n for n in graph_def.node if n.op == 'Const']
        for n in weight_nodes:
            print("Name of the node - %s" % n.name)
            print("Value - " )
            print(tensor_util.MakeNdarray(n.attr['value'].tensor))
        '''


delf=CNN("/home/claudio/Escritorio/Matching descriptor/delf/saved_model.pb")

#delf = hub.load("./home/claudio/Escritorio/Matching descriptor/delf/tfhub_module.pb").signatures['default']

flag_match=False
flag_result=False
tic=time.time()
distancia_min=100000000
k_max=0
path_imagenes='/home/claudio/Escritorio/Matching descriptor/Imagenes'
lista_imagenes = os.listdir(path_imagenes)

img1 = cv2.imread('lubriderm_m_acr.jpg') # queryImage
img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
sift = cv2.xfeatures2d.SIFT_create()
kp1, des1 = sift.detectAndCompute(img1,None)

for path_image in lista_imagenes:

    img2 = cv2.imread(path_imagenes + "/" + path_image) #trainImage
    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    # Initiate SIFT detector

    # find the keypoints and descriptors with SIFT
    kp2, des2 = sift.detectAndCompute(img2,None)

    # FLANN parameters
    FLANN_INDEX_KDTREE = 1
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks=50)   # or pass empty dictionary
    flann = cv2.FlannBasedMatcher(index_params,search_params)
    matches = flann.knnMatch(des1,des2,k=2)

    # Need to draw only good matches, so create a mask
    matchesMask = [[0,0] for i in range(len(matches))]
    k=0
    acum_distance=0
    # ratio test as per Lowe's paper
    for i,(m,n) in enumerate(matches):
        if m.distance < 0.6*n.distance:
            matchesMask[i]=[1,0]
            k=k+1
            acum_distance = acum_distance + m.distance
            if distancia_min>m.distance:
                distancia_min=m.distance

            flag_match=True
    
    
    if flag_match:
        #k = k/acum_distance
        #distancia_min=100000000
        flag_match=False
        flag_result=True

        if k_max<k:
            k_max=k
            image_match=img2 
            kp2_match=kp2
            des2_match=des2
            matches_m=matches
            matchesMask_m=matchesMask


if flag_result:
    draw_params = dict(matchColor = (0,255,0),
                    singlePointColor = (255,0,0),
                    matchesMask = matchesMask_m,
                    flags = cv2.DrawMatchesFlags_DEFAULT)

    tac=time.time()


    img3 = cv2.drawMatchesKnn(img1,kp1,image_match,kp2_match,matches_m,None,**draw_params)


    tiempo_transcurrido=tac-tic
    print('tiempo transcurrido: ', tiempo_transcurrido)
    plt.imshow(img3,),plt.show()