import numpy as np

from skimage.transform import hough_line, hough_line_peaks
from skimage.feature import canny
from skimage.draw import line
import matplotlib.pyplot as plt
from matplotlib import cm
import cv2
import imutils
from skimage import transform
import glob

if __name__ == '__main__':
    filename = './imagenes_originales/imagen_02.jpg'
    image_original = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
    num_rows, num_cols = image_original.shape[:2]
    image = cv2.imread(filename)
    image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    image = imutils.auto_canny(image)
    tested_angles = np.linspace(-np.pi / 2, np.pi / 2, 360, endpoint=False)
    h, theta, d = hough_line(image)
    y_0 = []
    y_1 = []
    x_0 = []
    x_1 = []

    origin = np.array((0, image.shape[1]))
    origin_x = np.array((0, image.shape[0]))
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, min_distance=100, min_angle=70)):
        y0, y1 = (dist - origin * np.cos(angle)) / np.sin(angle)
        #horizontales
        if y0>0 and y0<num_rows and y1>0 and y1<num_rows:
            y_0.append(int(y0))
            y_1.append(int(y1))
        x0, x1 = (dist - origin * np.sin(angle)) / np.cos(angle)
        if (x0>0 and x0<num_cols) or (x1>0 and x1<num_cols):
            y0 = origin_x[0]
            y1 = origin_x[1]

            x_0.append([int(x0),y0])
            x_1.append([int(x1),y1])

    p14_i = y_0.index(min(y_0))
    p23_i = y_0.index(max(y_0))
    p1 = [0, y_0[p14_i]]
    p2 = [0, y_0[p23_i]]
    p3 = [num_cols-1, y_1[p23_i]]
    p4 = [num_cols-1, y_1[p14_i]]
    #dy = max(abs(y_0[p14_i]-y_0[p23_i]),abs(y_1[p14_i]-y_1[p23_i]))
    #dx = int(max(np.sqrt(((num_cols-1)**2)+((p1[1]-p2[1])**2)),np.sqrt(((num_cols-1)**2)+((p3[1]-p4[1])**2))))
    y_ref_up = min(p1[1],p4[1])
    y_ref_down = min(p2[1],p3[1])
    src = np.array([[0, y_ref_up], [0, y_ref_down], [num_cols-1, y_ref_down], [num_cols-1, y_ref_up]])
    dst = np.array([p1,p2,p3,p4])

    tform3 = transform.ProjectiveTransform()
    tform3.estimate(src, dst)
    warped = transform.warp(image_original, tform3, output_shape=(num_rows, num_cols))

    fig, ax = plt.subplots(nrows=4, figsize=(8, 3))

    ax[0].imshow(image_original, cmap=plt.cm.gray)
    ax[0].plot((dst[:, 0]), (dst[:, 1]), '.r')
    ax[1].imshow(image_original, cmap=plt.cm.gray)
    for i in range(len(y_0)):
        ax[1].plot(origin, (y_0[i], y_1[i]), '-r')
    for a in ax:
        a.axis('off')
    ax[2].imshow(image_original, cmap=plt.cm.gray)
    for i in range(len(x_0)):
        ax[2].plot((x_0[i][0], x_1[i][0]), (x_0[i][1], x_1[i][1]), '-r')
    for a in ax:
        a.axis('off')
    ax[3].imshow(warped, cmap=plt.cm.gray)

    plt.tight_layout()
    plt.show()

    #se muestra el resultado
