import cv2
import numpy as np
import matplotlib.pyplot as plt
import imutils
from PIL import ImageEnhance
from PIL import Image
from PIL import ImageFilter
if __name__ == "__main__":
    n = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10']
    for i in n:
        imagen = 'imagen_'+i
        filename = './imagenes_originales/'+imagen+'.jpg'
        #se carga la imagen
        image_original = cv2.cvtColor(cv2.imread(filename), cv2.COLOR_BGR2RGB)
        num_rows, num_cols = image_original.shape[:2]
        #se pasa la imagen a pillow, se mejora el contraste y se pasa de vuelta a opencv
        src = Image.fromarray(image_original)
        src = ImageEnhance.Contrast(src).enhance(5.0)
        src = np.asarray(src)
        src = cv2.cvtColor(src, cv2.COLOR_RGB2GRAY)
        #se aplica la deteccion de bordes
        dst = imutils.auto_canny(src)
        cdstP = dst
        #se elimina la zona superior e inferior para tener menos datos de techo y piso
        dst[0:int(num_rows/10)] = 0
        dst[int(9*num_rows/10):] = 0
        #se obtienen lineas mediante el algoritmo HoughLines probabilistico
        linesP = cv2.HoughLinesP(dst, 1, np.pi / 180, 50, 2, 500, 30)
        angulos = []
        if linesP is not None:
            for i in range(0, len(linesP)):
                l = linesP[i][0]
                #se calcula el angulo de rotacion dado por cada linea
                if l[2]>=l[0]:
                    dx = l[2]-l[0]
                    dy = l[3]-l[1]
                elif l[2]<l[0]:
                    dx = l[0]-l[2]
                    dy = l[1]-l[3]
                degree = 180.0 * np.arctan2(dy, dx) / np.pi
                #se limitan las lineas, para que no afecten las verticales
                if abs(degree)<30:
                    #se agrega el angulo a la lista
                    angulos.append(degree)
                    #opcional, se dibujan las lineas en la imagen de bordes
                    cv2.line(cdstP, (l[0], l[1]), (l[2], l[3]), (255,0,0), 3, cv2.LINE_AA)
        #se calcula cual angulo contiene mas datos en su vecindad
        delta_angle = 0.1
        frecuencia_angulo = []
        for i in angulos:
            tmp = 0
            for j in angulos:
                if abs(j-i)<=delta_angle:
                    tmp+=1
            frecuencia_angulo.append(tmp)
        #se obtiene el angulo de rotacion
        #por el tipo de rotacion en imutils se debe rotar hacia el lado contrario
        degree = angulos[frecuencia_angulo.index(max(frecuencia_angulo))] *-1.0
        #se utiliza imutils para rotar la imagen
        rotated = imutils.rotate_bound(image_original, degree)
        #se muestra el resultado
        (fig, (ax1, ax2, ax3)) = plt.subplots(1, 3, figsize=(15, 15))
        ax1.title.set_text('Source')
        ax1.imshow(image_original)
        ax2.title.set_text('Detected Lines - Probabilistic Line Transform')
        ax2.imshow(cdstP)
        ax3.title.set_text('Imagen rotada')
        ax3.imshow(rotated)
        plt.show()
