#!/usr/bin/env python

import argparse
import os
import sys

import keras
import tensorflow as tf
import cv2
import numpy as np
import matplotlib.pyplot as plt

from object_detector_retinanet.keras_retinanet import models
from object_detector_retinanet.keras_retinanet.preprocessing.csv_generator import CSVGenerator
from object_detector_retinanet.keras_retinanet.utils.predict_iou import deteccion_producto
from object_detector_retinanet.keras_retinanet.utils.keras_version import check_keras_version
from object_detector_retinanet.utils import image_path, annotation_path, root_dir
from object_detector_retinanet.keras_retinanet.matching import matching_lpips


def get_session():
    """ Construct a modified tf session.
    """
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)

class Image_detection():
    def __init__(self,model):
        
        self.model = model

    def start(self,img,text):

        # Blue color in BGR 
        color_bbox = (255, 0, 0) 
        color_text = (0,0, 0) 
        thickness_bbox = 8
        thickness_text = 15
        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 5

        detections = deteccion_producto(self.model,img)

        for bbox in detections:
        
            x1 = int(bbox[0])
            y1 = int(bbox[1])
            x2 = int(bbox[2])
            y2 = int(bbox[3])

            start_point = (x1,y1)
            end_point = (x2,y2)
            #match.start(img_crop)
            img = cv2.rectangle(img, start_point, end_point, color_bbox, thickness_bbox)

        textsize = cv2.getTextSize(text,font,font_scale,thickness_text)[0]
        textX = int((img.shape[1] - textsize[0]) / 2)
        textY = textsize[1]

        cv2.putText(img,text,(textX,textY),cv2.FONT_HERSHEY_SIMPLEX,font_scale,color_text,thickness_text)  
        return img


def main(args=None):
    # parse arguments
    model_path = "/home/claudio/Documentos/SKU110K/snapshots/iou_resnet50_csv_06.h5"

    path_imagenes_original ="/home/claudio/Escritorio/3R_mayor_3/test/perspectiva/"
    path_imagenes_0_1 ="/home/claudio/Escritorio/3R_mayor_3/test/5/"
    path_imagenes_0_5="/home/claudio/Escritorio/3R_mayor_3/test/10/"
    path_imagenes_0_7 ="/home/claudio/Escritorio/3R_mayor_3/test/20/"
    path_imagenes_1_5="/home/claudio/Escritorio/interacion_sharpening/test/1_5/"
    path_imagenes_2 ="/home/claudio/Escritorio/interacion_sharpening/test/2/"
    path_imagenes_3 ="/home/claudio/Escritorio/interacion_sharpening/test/3/"

    lista_imagenes = os.listdir(path_imagenes_original)


    salida = "/home/claudio/Escritorio/3R_mayor_3/deteccion/"

    
    # Line thickness of 2 px 
    thickness = 3

    use_cpu = False

    gpu_num = str(0)

    if use_cpu:
        os.environ["CUDA_VISIBLE_DEVICES"] = str(666)
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = gpu_num
    tf.compat.v1.keras.backend.get_session()

    # load the model
    model = models.load_model(os.path.join(root_dir(), model_path), backbone_name='resnet50', convert=1, nms=False)
    image_detection = Image_detection(model)
    match = matching_lpips.match_lpips()
    i = 0
    e = 0

    font_scale = 6

    for path_imagen in lista_imagenes:

        img_original = cv2.imread(path_imagenes_original + path_imagen)
        img_perspectiva_0_1 = cv2.imread(path_imagenes_0_1 + path_imagen)
        img_perspectiva_0_5 = cv2.imread(path_imagenes_0_5 + path_imagen)
        img_perspectiva_0_7= cv2.imread(path_imagenes_0_7 + path_imagen)
        img_perspectiva_1_5 = cv2.imread(path_imagenes_1_5 + path_imagen)
        img_perspectiva_2 = cv2.imread(path_imagenes_2 + path_imagen)
        img_perspectiva_3 = cv2.imread(path_imagenes_3 + path_imagen)

        # start prediction
        h,w,d = img_original.shape

        img_original_detection = image_detection.start(img_original,"Original")
        img_perspectiva_0_1_detection= image_detection.start(img_perspectiva_0_1,"5")
        img_perspectiva_0_5_detection = image_detection.start(img_perspectiva_0_5, "10")
        img_perspectiva_0_7_detection = image_detection.start(img_perspectiva_0_7,"20")

        img_black = np.zeros((h, w, d),np.uint8)

        im_h1 = cv2.hconcat([img_original_detection, img_perspectiva_0_1_detection])
        im_h2 = cv2.hconcat([img_perspectiva_0_5_detection, img_perspectiva_0_7_detection])
        im_total = cv2.vconcat([im_h1,im_h2])
        
        cv2.imwrite(salida + path_imagen, im_total)


if __name__ == '__main__':
    main()
