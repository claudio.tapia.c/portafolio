import pandas as pd
import os
from numba import njit, jit, prange
import numpy as np
from functools import partial
import cv2

path_imagenes = "./annotations/"
path_base_2 = "./base_2/"
path_annotations_csv = "./annotations_train_part_2.csv"

lista_imagenes = os.listdir(path_imagenes)

@njit
def indices_(lista_imagenes_malas,imagenes_annotations):
    indices = []
    len_imagenes_malas = len(lista_imagenes_malas)
    len_imagen_annotations = len(imagenes_annotations)
    
    for index_imagen_mala in prange(len_imagenes_malas):
        for index_annotations in prange(len_imagen_annotations):
            if imagenes_annotations[index_annotations]==lista_imagenes_malas[index_imagen_mala]:
                indices.append(index_annotations)
    
    return indices


df = pd.read_csv(path_annotations_csv,header=None)



imagenes_annotations_filter = list(set(df[0].values.tolist()))


result = []
for base_2_image in imagenes_annotations_filter:

    img = cv2.imread(path_imagenes+base_2_image)
    cv2.imwrite(path_base_2 + base_2_image,img)
