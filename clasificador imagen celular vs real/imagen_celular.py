import numpy as np
import cv2
import os
from imageio import imread
from keras.models import load_model
from keras.applications.xception import preprocess_input
from keras.preprocessing import image
from random import randint


target_class =os.listdir('./data celu/TRAIN')

model = load_model('Pesos/Xception(mejor).hdf5')

image_size = 160

cap = cv2.VideoCapture(0)
cap.set(3, 640) #WIDTH
cap.set(4, 480) #HEIGHT

cascPath = './cv2/haarcascade_frontalface_alt2.xml'
images_reales_Path='./caras_reales/'
images_celular_Path='./caras_celular/'

while(True):
    # Capture frame-by-frame
    ret, frame = cap.read()

    # Our operations on the frame come here
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    face_cascade = cv2.CascadeClassifier(cascPath)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5,minSize=(140, 140),maxSize=(330, 330))

    # Display the resulting frame
    for (x,y,w,h) in faces:         
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = frame[y:y+h, x:x+w]
        roi_color=cv2.resize(roi_color,(image_size,image_size))
        cv2.imwrite('imagen.jpg',roi_color)
        img = image.load_img('imagen.jpg', target_size=(image_size, image_size))
        im_to_net = image.img_to_array(img)
        #normaliza las imagenes para la red Xception
        im = preprocess_input(im_to_net)
        
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)
        
        prediccion= model.predict(np.reshape(im, [-1,image_size,image_size,3]))
        
        if prediccion[0,1]>prediccion[0,0]:
            if  np.max(prediccion, -1)[0] >= 0.995:
     
                cv2.putText(frame,target_class[np.argmax((prediccion), -1)[0]],(x,y),cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,255),2)
                
                
            else:
                
                acum=0    
        else:
            cv2.putText(frame,target_class[0],(x,y),cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,255),2)
            
        if cv2.waitKey(1) & 0xFF == ord('s'):
            if np.argmax((prediccion), -1)[0]==1:
                cv2.imwrite(images_celular_Path + str(randint(0,99999999)) +"hola"+".jpg",roi_color)
            else:
                cv2.imwrite(images_reales_Path + str(randint(0,99999999)) +"hola" +".jpg",roi_color)
                    
        
               
        
    cv2.imshow('frame',frame)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()