from object_detector_retinanet.keras_retinanet.bin.SKU110K import Detector

image_path = "gondola.jpg"
pickle_path = "bbox.pickle"
model_path = "./snapshots/iou_resnet50_csv_06.h5"

if __name__ == "__main__":
	detector = Detector(model_path, pickle_path)

	img, bbox = detector.start(image_path)
	print(bbox)