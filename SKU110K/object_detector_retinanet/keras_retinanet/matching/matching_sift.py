import numpy as np
import cv2
import matplotlib.pyplot as plt
import time
import os
import imghdr


def match_sift(img):

    k_max = 0
    ratio=0.745
    MIN_MATCH_COUNT = 8
    FLANN_INDEX_KDTREE = 1
    flag_match = False

    path_imagenes='/home/claudio/Escritorio/Matching descriptor/planograma_bucal'
    lista_imagenes = os.listdir(path_imagenes)

    # FLANN parameters
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks=50)   # or pass empty dictionary
    flann = cv2.FlannBasedMatcher(index_params,search_params)
    sift = cv2.xfeatures2d.SIFT_create()

    tic = time.time()
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    kp1, des1 = sift.detectAndCompute(img,None)

    for path_image in lista_imagenes:

        img2 = cv2.imread(path_imagenes + "/" + path_image) #trainImage
        img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
        # find the keypoints and descriptors with SIFT
        kp2, des2 = sift.detectAndCompute(img2,None)
        matches = flann.knnMatch(des1,des2,k=2)

        good = []
        dst_pts = []
        src_pts = []
        
        for i,(m,n) in enumerate(matches):
            if m.distance < ratio*n.distance:
                good.append(m)
                dst_pts.append(kp1[m.queryIdx].pt)
                src_pts.append(kp2[m.trainIdx].pt)

        if len(good)>MIN_MATCH_COUNT:
            
            dst_pts= np.float32(dst_pts).reshape(-1,1,2)
            src_pts= np.float32(src_pts).reshape(-1,1,2)
            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
            matchesMask_ = mask.ravel().tolist()
            k=0
            for e in matchesMask_:
                if e==1:
                    k=k+1

            if k_max<k:
                k_max=k
                image_match=img2 
                path_image_m = path_image     
            
        else:
            #print( "\n Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT))
            matchesMask_ = None
    
    if k_max >= 1:
        print(k_max)
        print(path_image_m)
        fig, axes = plt.subplots(1, 2)
        fig.add_subplot(1, 2, 1)
        plt.imshow(img)
        fig.add_subplot(1, 2, 2)
        plt.imshow(image_match)
        fig.savefig('figures/' + path_image_m)
