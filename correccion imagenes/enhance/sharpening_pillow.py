import matplotlib.pyplot as plt
import cv2
from PIL import Image
from PIL import ImageEnhance
import numpy as np
import os
ruta_imagenes = './set_pruebas/'
ruta_salida = '/home/claudio/Imágenes/20_gondolas/test/sharpening_pillow/'

path_images = os.listdir(ruta_imagenes)


for path_image in path_images:
    print(ruta_imagenes + path_image)
    image = cv2.cvtColor(cv2.imread(ruta_imagenes + path_image), cv2.COLOR_BGR2RGB)
    src = Image.fromarray(image)
    result = ImageEnhance.Sharpness(src).enhance(2)
    cv2.imwrite(ruta_salida + path_image, cv2.cvtColor(np.asarray(result), cv2.COLOR_RGB2BGR))


