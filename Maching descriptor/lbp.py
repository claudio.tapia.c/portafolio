# OpenCV bindings
import cv2
# To performing path manipulations 
import os
# Local Binary Pattern function
from skimage.feature import local_binary_pattern
# To calculate a normalized histogram 
from scipy.stats import itemfreq
from sklearn.preprocessing import normalize
# Utility package -- use pip install cvutils to install
# To read class from file
import csv
import numpy as np
import tensorflow as tf
from numpy import linalg as LA
import imagehash
from PIL import Image

def norm_img(img):
    img_r = img[:,:,0]
    img_g = img[:,:,1]
    img_b = img[:,:,2]

    img_r= 255*((img_r-img_r.min())/(img_r.max()-img_r.min()))
    img_g= 255*((img_g-img_g.min())/(img_g.max()-img_g.min()))
    img_b= 255*((img_b-img_b.min())/(img_b.max()-img_b.min()))

    img[:,:,0] = img_r.astype(int)
    img[:,:,1] = img_g.astype(int)
    img[:,:,2] = img_b.astype(int)

    return img

score_min = 100000000000000000
radius = 3
# Number of points to be considered as neighbourers 
no_points = 8 * radius

path_imagenes='/home/claudio/Escritorio/Matching descriptor/imagenes_vgg_sift_cremas'
test_image = './simonds_dermo-cream-humectante.jpg'

lista_imagenes = os.listdir(path_imagenes)

path_image_test_list_split = []
for path_image_test in lista_imagenes:

    path_image_test_split = path_image_test.split("_")
    path_image_test_split = path_image_test_split[0] + "_" + path_image_test_split[1]+ ".jpg"
    path_image_test_list_split.append(path_image_test_split)

color = ('r','g','b')

# Read the image
img1 = cv2.imread(test_image)
img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
img1 = norm_img(img1)
# Convert to grayscale as LBP works on grayscale image
im_gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)

lbp = local_binary_pattern(im_gray, no_points, radius, method='uniform')
# Calculate the histogram
hist1 = itemfreq(lbp.ravel())
# Normalize the histogram

hist_color1 = []
    
for i,col in enumerate(color):
    hist_color1.append(cv2.calcHist([img1],[i],None,[256],[0,255]))

hist_color1 = np.concatenate((hist_color1[0], hist_color1[1], hist_color1[2]), axis=None)

hist1=hist1/LA.norm(hist1)
hist_color1=hist_color1/LA.norm(hist_color1)

hash = imagehash.average_hash(Image.open(test_image))

list_k_path = []
list_k_path_falso = []

for train_image, train_image_split in zip(lista_imagenes, path_image_test_list_split):
    # Read the image
    img2 = cv2.imread(path_imagenes + '/' + train_image)
    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
    img2 = norm_img(img2)
     # Convert to grayscale as LBP works on grayscale image
    im_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

    hist_color2 = []

    for i,col in enumerate(color):
        hist_color2.append(cv2.calcHist([img2],[i],None,[256],[0,255]))

    hist_color2 = np.concatenate((hist_color2[0], hist_color2[1], hist_color2[2]), axis=None)

    # Uniform LBP is used
    lbp = local_binary_pattern(im_gray, no_points, radius, method='uniform')
    # Calculate the histogram
    hist2 = itemfreq(lbp.ravel())
    # Normalize the histogram
    hist2=hist2/LA.norm(hist2)
    hist_color2 = hist_color2/LA.norm(hist_color2)

    
    otherhash = imagehash.average_hash(Image.open(path_imagenes + '/' + train_image))

    # Calculate the chi-squared distance and the sort the values
    #score_color = cv2.compareHist(np.array(hist_color1, dtype=np.float32),np.array(hist_color2, dtype=np.float32),cv2.HISTCMP_INTERSECT)
    #score_lbp = cv2.compareHist(np.array(hist1, dtype=np.float32), np.array(hist2, dtype=np.float32), cv2.HISTCMP_INTERSECT)
    
    score_color = LA.norm(hist2-hist1)
    score_lbp = LA.norm(hist_color2-hist_color1)

    score = score_lbp
    
    #print(score)

    if score < score_min:
        score_min = score
        train_image_match = train_image
    
    dict_match_path = dict(Imagen_planograma = train_image, Match = score_min)
    dict_match_path_falso = dict(Imagen_planograma = train_image_split, Match = score_min)
    list_k_path.append(dict_match_path)
    list_k_path_falso.append(dict_match_path_falso)

print(train_image_match)

list_k_path = sorted(list_k_path, key = lambda i: i["Match"],reverse=False)
list_k_path_falso = sorted(list_k_path_falso, key = lambda i: i["Match"],reverse=False)
ranking = [lista["Imagen_planograma"] for lista in list_k_path]
ranking_falso = [lista["Imagen_planograma"] for lista in list_k_path_falso]

for pos in ranking:
    print(pos)