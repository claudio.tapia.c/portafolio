import matplotlib.pyplot as plt
import cv2
import glob
from PIL import Image
from PIL import ImageEnhance
import numpy as np
import os
from skimage.transform import hough_line, hough_line_peaks
from skimage.feature import canny
from skimage import transform
from skimage.draw import line
from skimage import io
from matplotlib import cm
import imutils
from skimage import data
from skimage.filters import unsharp_mask
from skimage import img_as_ubyte


def proyeccion(img):
    num_rows, num_cols = img.shape[:2]
    img_original = img.copy()
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    img = imutils.auto_canny(img)
    tested_angles = np.linspace(-np.pi / 2, np.pi / 2, 360, endpoint=False)
    h, theta, d = hough_line(img)
    y_0 = []
    y_1 = []
    origin = np.array((0, img.shape[1]))
    origin_x = np.array((0, img.shape[0]))
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, min_distance=100, min_angle=70)):
        y0, y1 = (dist - origin * np.cos(angle)) / np.sin(angle)
        #horizontales
        if y0>0 and y0<num_rows and y1>0 and y1<num_rows and abs(y1-y0)<500:
            y_0.append(int(y0))
            y_1.append(int(y1))

    p14_i = y_0.index(min(y_0))
    p23_i = y_0.index(max(y_0))
    p1 = [0, y_0[p14_i]]
    p2 = [0, y_0[p23_i]]
    p3 = [num_cols-1, y_1[p23_i]]
    p4 = [num_cols-1, y_1[p14_i]]
    y_ref_up = min(p1[1],p4[1])
    y_ref_down = min(p2[1],p3[1])
    src = np.array([[0, y_ref_up], [0, y_ref_down], [num_cols-1, y_ref_down], [num_cols-1, y_ref_up]])
    dst = np.array([p1,p2,p3,p4])

    tform3 = transform.ProjectiveTransform()
    tform3.estimate(src, dst)
    warped = transform.warp(img_original, tform3, output_shape=(num_rows, num_cols))
    warped = img_as_ubyte(warped)
    open_cv_image = warped[:, :, ::-1]
    return open_cv_image


def sharpening_pillow(img,ratio):
    img = Image.fromarray(img)
    img = ImageEnhance.Contrast(img).enhance(ratio)
    img = np.asarray(img) 
    open_cv_image = img[:, :, ::-1].copy() 
    return open_cv_image


def sharpening_skimage(img, ratio):
    img = unsharp_mask(img, radius=ratio, amount=1)
    img = img_as_ubyte(img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img


def contraste(img, ratio):
    img = Image.fromarray(img)
    img = ImageEnhance.Contrast(img).enhance(ratio)
    img = np.asarray(img)
    #open_cv_image = img[:, :, ::-1].copy() 
    return img


if __name__ == '__main__':

    ruta_imagenes = "./set_pruebas/"
    ruta_salida_perspectiva  = '/home/claudio/Escritorio/20_gondolas_contraste/test/perspectiva/'
    ruta_salida_contraste_0_8  = '/home/claudio/Escritorio/20_gondolas_contraste/test/0_8/'
    ruta_salida_contraste_0_99 = '/home/claudio/Escritorio/20_gondolas_contraste/test/0_99/'
    ruta_salida_contraste_1_5 = '/home/claudio/Escritorio/20_gondolas_contraste/test/1_5/'
    ruta_salida_contraste_2 = '/home/claudio/Escritorio/20_gondolas_contraste/test/2/'

    path_images = os.listdir(ruta_imagenes)


    for path_image in path_images:
        
        img_original = cv2.imread(ruta_imagenes + path_image)
        img_proyeccion = proyeccion(img_original)
        
        img_proyeccion_contraste_0_8 = contraste(img_proyeccion, 0.8)
        img_proyeccion_contraste_0_99= contraste(img_proyeccion, 0.99)
        img_proyeccion_contraste_1_5= contraste(img_proyeccion, 1.5)
        img_proyeccion_contraste_2= contraste(img_proyeccion, 2)

        img_contraste_0_8 = sharpening_skimage(img_proyeccion_contraste_0_8,3)
        img_contraste_0_99 = sharpening_skimage(img_proyeccion_contraste_0_99,3)
        img_contraste_1_5 = sharpening_skimage(img_proyeccion_contraste_0_99,3)
        img_contraste_2 = sharpening_skimage(img_proyeccion_contraste_2,3)

        cv2.imwrite(ruta_salida_perspectiva + path_image, img_proyeccion)
        cv2.imwrite(ruta_salida_contraste_0_8 + path_image, img_contraste_0_8)
        cv2.imwrite(ruta_salida_contraste_0_99 + path_image, img_contraste_0_99)
        cv2.imwrite(ruta_salida_contraste_1_5 + path_image, img_contraste_1_5)
        cv2.imwrite(ruta_salida_contraste_2 + path_image, img_contraste_2)


