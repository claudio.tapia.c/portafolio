#!/usr/bin/env python

import argparse
import os
import sys

import keras
import tensorflow as tf
import cv2

import pickle

from object_detector_retinanet.keras_retinanet import models
from object_detector_retinanet.keras_retinanet.preprocessing.csv_generator import CSVGenerator
from object_detector_retinanet.keras_retinanet.utils.predict_iou_BIG import deteccion_producto
from object_detector_retinanet.keras_retinanet.utils.keras_version import check_keras_version
from object_detector_retinanet.utils import image_path, annotation_path, root_dir
from object_detector_retinanet.keras_retinanet.matching import matching_lpips

def get_session():
    """ Construct a modified tf session.
    """
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)


def main(args=None):
    # parse arguments
    model_path = "/home/claudio/Documentos/SKU110K/snapshots/iou_resnet50_csv_06.h5"
    path_imagenes ="/home/claudio/Documentos/SKU110K/imagenes gondolas/"
    path_imagenes_cortadas ="/home/claudio/Documentos/SKU110K/imagenes_cortadas/"

    lista_imagenes = os.listdir(path_imagenes)
    # Blue color in BGR 
    color = (255, 0, 0) 
    # Line thickness of 2 px 
    thickness = 1

    use_cpu = False

    gpu_num = str(0)

    if use_cpu:
        os.environ["CUDA_VISIBLE_DEVICES"] = str(666)
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = gpu_num
    tf.compat.v1.keras.backend.get_session()

    # load the model
    model = models.load_model(os.path.join(root_dir(), model_path), backbone_name='resnet50', convert=1, nms=False)
    match = matching_lpips.match_lpips()
    i = 0
    e = 0

    for path_imagen in lista_imagenes:

        i+=1
        img = cv2.imread(path_imagenes + path_imagen)
        cv2.imwrite("xxx.jpg",img)
        img_copy=img.copy()
        # start prediction
        image_detections = deteccion_producto(model,"xxx.jpg")

        with open('bbox.pickle', 'wb') as f:
            pickle.dump(image_detections, f)
        try:
            os.stat(path_imagenes_cortadas + str(i) + "/")
        except:
            os.mkdir(path_imagenes_cortadas + str(i) + "/")

        cv2.imwrite(path_imagenes_cortadas + str(i) + "/" + str(i) + ".jpg", img)

        for x1,y1,x2,y2 in zip(image_detections['x1'], image_detections['y1'], image_detections['x2'], image_detections['y2']):
            e+=1
            try:
                os.stat(path_imagenes_cortadas + str(i) + "/cortadas/")
            except:
                os.mkdir(path_imagenes_cortadas + str(i) + "/cortadas/" )

            #x1 = int(image_box[0])
            #y1 = int(image_box[1])
            #x2 = int(image_box[2])
            #y2 = int(image_box[3])

            size = img_copy.shape
    
            h = size[0]
            w = size[1]
            d = size[2]

            offset_x = int(w/70)
            offset_y = int(h/33)
            
            #offset_x = 0
            #offset_y = 0
            y1_arriba = y1-offset_y
            #y2_abajo = y2+1+offset_y
            y2_abajo = y2+1
            x1_izquierda = x1-offset_x
            x2_derecha = x2+1+offset_x

            if y1_arriba < 0:
                y1_arriba = 0

            if x1_izquierda < 0:
                x1_izquierda = 0
            
            if y2_abajo > h:
                y2_abajo = h

            if x2_derecha > w:
                x2_derecha = w

            start_point = (x1,y1)
            end_point = (x2,y2)
            img_crop = img[y1_arriba:y2_abajo, x1_izquierda:x2_derecha]
            cv2.imwrite(path_imagenes_cortadas + str(i) + "/cortadas/" + str(e) + ".jpg" , img_crop)
            #match.start(img_crop)
            
            img_copy = cv2.rectangle(img_copy, start_point, end_point, color, thickness)

        cv2.imwrite(path_imagenes_cortadas + str(i) + "/" + str(i) + "_detecciones_.jpg" , img_copy)


if __name__ == '__main__':
    main()
