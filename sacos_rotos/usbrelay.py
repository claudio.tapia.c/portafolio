#!/usr/bin/python
# -*- coding: utf-8 -*-

import usbrelay_py
import time
import argparse
import sys

def main():

    if args.debug:
        print("Count: ",count)
        print("Boards: ",boards)

    for board in boards:
        usbrelay_py.board_control(board[0],args.relay,args.state)
    

    
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--relay", type=int,
                        help="choose relay")
    parser.add_argument("--state", type=int, choices=[0, 1],
                        help="time of pulse in seconds")

    parser.add_argument("--debug", type=int, choices=[0, 1], default=0,
                        help="choose relay")

    args = parser.parse_args()

    count = usbrelay_py.board_count()
    boards = usbrelay_py.board_details()
    
    exit(main())
    


