import numpy as np
import cv2
import matplotlib.pyplot as plt
import time
import os
import imghdr
from pandas import ExcelWriter
from openpyxl import load_workbook
import pandas as pd
import json
from numpy import linalg as LA

VP = 0
VN = 0
FP = 0
FN = 0

ratio=0.745

k_max = 0

TOP = 10

MIN_MATCH_COUNT = 1
FLANN_INDEX_KDTREE = 1

delimitador = ","

path_imagenes_planograma='/home/claudio/Escritorio/Matching descriptor/planograma_bucal'
lista_imagenes_planograma = os.listdir(path_imagenes_planograma)

path_imagenes_test = '/home/claudio/Escritorio/Matching descriptor/cortadas_gondola_bucal_vespucio'
lista_imagenes_test = os.listdir(path_imagenes_test)

archivo= "resultados_matching_ranking_planograma_in_bucal_vespucio.xlsx"

if not os.path.isfile(archivo):
    df = pd.DataFrame({'Producto': [],
                       'Ranking': [],
                       'Estado': [],
                       '1 SIFT':[],
                       '2 SIFT':[],
                       '3 SIFT':[],
                       '4 SIFT':[],
                       '5 SIFT':[],
                       '6 SIFT':[],
                       '7 SIFT':[],
                       '8 SIFT':[],
                       '9 SIFT':[],
                       '10 SIFT':[]})
        
    writer = ExcelWriter(archivo)
    df.to_excel(writer, 'Hoja', index=False)
    writer.save()

path_image_test_list_split = []
for path_image_test in lista_imagenes_test:

    path_image_test_split = path_image_test.split("_")
    path_image_test_split = path_image_test_split[0] + "_" + path_image_test_split[1]+ ".jpg"
    path_image_test_list_split.append(path_image_test_split)

print(path_image_test_list_split)

# FLANN parameters
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 10)
search_params = dict(checks=50)   # or pass empty dictionary
flann = cv2.FlannBasedMatcher(index_params,search_params)
sift = cv2.xfeatures2d.SIFT_create()

print("\n**********************************************************************")

lists_k_match_path = []

def distancia_interseccion_histograma(hist1,hist2):
    size = hist1.size
    s=0
    print(size)
    for i in range(size):
        d=sum(min(hist1[i],hist2[i]))
        s+=d
    return s

j = 0

for path_image_planograma in lista_imagenes_planograma:

    j+=1

    img1 = cv2.imread(path_imagenes_planograma + "/" + path_image_planograma) 
    tic = time.time()
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
    kp1, des1 = sift.detectAndCompute(img1,None)

    img1_r = img1[:,:,0]
    img1_g = img1[:,:,1]
    img1_b = img1[:,:,2]

    img1_r= 255*((img1_r-img1_r.min())/(img1_r.max()-img1_r.min()))
    img1_g= 255*((img1_g-img1_g.min())/(img1_g.max()-img1_g.min()))
    img1_b= 255*((img1_b-img1_b.min())/(img1_b.max()-img1_b.min()))

    img1[:,:,0] = img1_r.astype(int)
    img1[:,:,1] = img1_g.astype(int)
    img1[:,:,2] = img1_b.astype(int)

    list_k_path = []
    list_k_path_falso = []
    hist1 = []
    color = ('r','g','b')
    
    for i,col in enumerate(color):
        hist1.append(cv2.calcHist([img1],[i],None,[128],[0,256]))

    hist1 = np.concatenate((hist1[0], hist1[1], hist1[2]), axis=None)

    hist1=hist1/LA.norm(hist1)

    for path_image_test_split, path_image_test in zip(path_image_test_list_split, lista_imagenes_test):


        img2 = cv2.imread(path_imagenes_test + "/" + path_image_test)
        img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
        img2_r = img2[:,:,0]
        img2_g = img2[:,:,1]
        img2_b = img2[:,:,2]

        img2_r= 255*((img2_r-img2_r.min())/(img2_r.max()-img2_r.min()))
        img2_g= 255*((img2_g-img2_g.min())/(img2_g.max()-img2_g.min()))
        img2_b= 255*((img2_b-img2_b.min())/(img2_b.max()-img2_b.min()))

        img2[:,:,0] = img2_r.astype(int)
        img2[:,:,1] = img2_g.astype(int)
        img2[:,:,2] = img2_b.astype(int)

        # find the keypoints and descriptors with SIFT
        kp2, des2 = sift.detectAndCompute(img2,None)
        matches = flann.knnMatch(des1,des2,k=2)

        hist2 = []

        for i,col in enumerate(color):
            hist2.append(cv2.calcHist([img2],[i],None,[128],[0,256]))

        hist2 = np.concatenate((hist2[0], hist2[1], hist2[2]), axis=None)

        hist2=hist2/LA.norm(hist2)

        dist = cv2.compareHist(hist1,hist2,cv2.HISTCMP_CORREL)

        good = []
        dst_pts = []
        src_pts = []
        acum_distancia = 0
        dict_match_path = {}

        # ratio test as per Lowe's paper
        
        for i,(m,n) in enumerate(matches):
            if m.distance < n.distance*ratio:
            
                acum_distancia = acum_distancia + m.distance
                good.append(m)
                dst_pts.append(kp1[m.queryIdx].pt)
                src_pts.append(kp2[m.trainIdx].pt)

        if len(good) > MIN_MATCH_COUNT:
            
            dst_pts= np.float32(dst_pts).reshape(-1,1,2)
            src_pts= np.float32(src_pts).reshape(-1,1,2)
            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
            matchesMask_ = mask.ravel().tolist()
            #print(M)
            k=0
    
            for e in matchesMask_:
                if e==1:
                    k=k+1
            
            dict_match_path = dict(Imagen_planograma = path_image_test, Match = k)
            dict_match_path_falso = dict(Imagen_planograma = path_image_test_split, Match = k)
            list_k_path.append(dict_match_path)
            list_k_path_falso.append(dict_match_path_falso)

            if k_max < k:
                k_max = k
                image_match = img2 
                kp2_match = kp2
                des2_match = des2
                matches_m = good
                matchesMask_m = matchesMask_
                path_image_match_split = path_image_test_split
                path_image_match = path_image_test   
                acum_distancia_anterior = acum_distancia
                
        else:
            #print( "\n Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT))
            matchesMask_ = None
        
    list_k_path = sorted(list_k_path, key = lambda i: i["Match"],reverse=True)
    list_k_path_falso = sorted(list_k_path_falso, key = lambda i: i["Match"],reverse=True)

    #list_k_path = list_k_path[0:TOP]
    #list_k_path_falso = list_k_path_falso[0:TOP]
    ranking = [lista["Imagen_planograma"] for lista in list_k_path]
    ranking_falso = [lista["Imagen_planograma"] for lista in list_k_path_falso]

    try:
        pos_caras =[]
        pos_anterior = 0

        while True:
            pos = ranking_falso.index(path_image_planograma, pos_anterior) + 1
            pos_caras.append(pos)
            pos_anterior = pos
            flag_detect = True
            estado = "1"
    except:
        if pos_caras==[]:
            pos = 0
            estado = "0"
            flag_detect = False

    if pos_caras==[]:
        pos_caras_delimitador = "0"
    else:

        pos_caras_delimitador = delimitador.join(map(str, pos_caras))
    print("La posicion es", pos_caras)

    dict_k_match_path = {"Path image test": path_image_planograma,"Pos":pos, "Ranking":list_k_path[0:TOP]}
    lists_k_match_path.append(dict_k_match_path)

    doc=load_workbook(archivo)
    hoja = doc.get_sheet_by_name(doc.get_sheet_names()[0])
    hoja.cell(row = j+1, column = 1).value = path_image_planograma.split(".jpg")[0]
    hoja.cell(row = j+1, column = 2).value = pos_caras_delimitador
    hoja.cell(row = j+1, column = 3).value = estado
    hoja.cell(row = j+1, column = 4).value = ranking[0]
    hoja.cell(row = j+1, column = 5).value = ranking[1]
    hoja.cell(row = j+1, column = 6).value = ranking[2]
    hoja.cell(row = j+1, column = 7).value = ranking[3]
    hoja.cell(row = j+1, column = 8).value = ranking[4]
    hoja.cell(row = j+1, column = 9).value = ranking[5]
    hoja.cell(row = j+1, column = 10).value = ranking[6]
    hoja.cell(row = j+1, column = 11).value = ranking[7]
    hoja.cell(row = j+1, column = 12).value = ranking[8]
    hoja.cell(row = j+1, column = 13).value = ranking[9]
    
    doc.save(archivo)

    if k_max==0:
            
        if path_image_planograma in path_image_test_list_split:

            FN+=1
            #print("VN actual: ", VN)
        else:
            VN+=1
            #print("FN actual: ", FN)

    else:
        print("\nImagen test: ", path_image_planograma)
        print("imagen match: {}   num match: {} \n".format(path_image_match,k_max))

        if path_image_match_split == path_image_planograma:

            VP+=1
            #print("VP actual: ", VP)
            print("MATCH EXITOSO!!!!!!\n")
        else:
            FP+=1

            #print(lists_k_match_path)
            #print("FP actual: ", FP)

    k_max = 0

if VP == 0:
    precision = 0
    sensibilidad = 0
    recall = 0
else:
    precision = 100*VP/(VP+FP)
    recall = 100*VP/(VP+FN)

if (VP+VN) == 0:
    exactitud = 0
else:
    exactitud = 100*(VP+VN)/(VP+FN+FP+VN)

if precision*recall == 0:
    f1 = 0
else:
    f1 = (2*precision*recall)/(precision + recall)

if VN == 0:
    especificidad = 100
else:
    especificidad = 100-100*VN/(VN+FP)

doc=load_workbook(archivo)
hoja = doc.get_sheet_by_name(doc.get_sheet_names()[0])

#print("cantidad de imagenes: " + str(cant_im))
print("Verdaderos positivos: " + str(VP))
print("Falsos positivos: " + str(FP) )
print("Falsos negativos: " + str(FN))
print("Verdaderos negativos: " + str(VN))
print("\n")
print("1-Especificidad: " + str(especificidad)+"%")
print("Recall: " + str(recall)+"%")
print("Precision: " + str(precision)+"%")
print("Exactitud: " + str(exactitud)+"%")
print("F1: " + str(f1)+"%")
print("**********************************************************************")
print("\n \n")



VP = 0
VN = 0
FP = 0
FN = 0

with open("ranking.json", "w") as f:
    json.dump(lists_k_match_path, f, ensure_ascii=False, indent=2)