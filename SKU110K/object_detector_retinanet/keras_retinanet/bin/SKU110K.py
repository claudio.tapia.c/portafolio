#!/usr/bin/env python

import argparse
import os
import sys

import keras
import tensorflow as tf
import cv2
import numpy as np
from skimage.transform import hough_line, hough_line_peaks
from skimage import transform
import imutils
from skimage.filters import unsharp_mask
from skimage import img_as_ubyte

import pickle

from object_detector_retinanet.keras_retinanet import models
from object_detector_retinanet.keras_retinanet.preprocessing.csv_generator import CSVGenerator
from object_detector_retinanet.keras_retinanet.utils.predict_iou import deteccion_producto
from object_detector_retinanet.keras_retinanet.utils.keras_version import check_keras_version
from object_detector_retinanet.utils import image_path, annotation_path, root_dir


class Detector:

    def __init__(self, model_path, path_pickle_out):

        self.path_pickle_out = path_pickle_out
        self.__ratio = 1.2
        use_cpu = False
        gpu_num = str(0)

        if use_cpu:
            os.environ["CUDA_VISIBLE_DEVICES"] = str(666)
        else:
            os.environ["CUDA_VISIBLE_DEVICES"] = gpu_num

        tf.compat.v1.keras.backend.get_session()

        # load the model
        self.model = models.load_model(os.path.join(root_dir(), model_path), backbone_name='resnet50', convert=1, nms=False)


    def __proyeccion(self,img):
        num_rows, num_cols = img.shape[:2]
        img_original = img.copy()
        img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        img = imutils.auto_canny(img)
        tested_angles = np.linspace(-np.pi / 2, np.pi / 2, 360, endpoint=False)
        h, theta, d = hough_line(img)
        y_0 = []
        y_1 = []
        origin = np.array((0, img.shape[1]))
        origin_x = np.array((0, img.shape[0]))
        for _, angle, dist in zip(*hough_line_peaks(h, theta, d, min_distance=100, min_angle=70)):
            y0, y1 = (dist - origin * np.cos(angle)) / np.sin(angle)
            #horizontales
            if y0>0 and y0<num_rows and y1>0 and y1<num_rows and abs(y1-y0)<500:
                y_0.append(int(y0))
                y_1.append(int(y1))

        p14_i = y_0.index(min(y_0))
        p23_i = y_0.index(max(y_0))
        p1 = [0, y_0[p14_i]]
        p2 = [0, y_0[p23_i]]
        p3 = [num_cols-1, y_1[p23_i]]
        p4 = [num_cols-1, y_1[p14_i]]
        y_ref_up = min(p1[1],p4[1])
        y_ref_down = min(p2[1],p3[1])
        src = np.array([[0, y_ref_up], [0, y_ref_down], [num_cols-1, y_ref_down], [num_cols-1, y_ref_up]])
        dst = np.array([p1,p2,p3,p4])

        tform3 = transform.ProjectiveTransform()
        tform3.estimate(src, dst)
        warped = transform.warp(img_original, tform3, output_shape=(num_rows, num_cols))
        warped = img_as_ubyte(warped)
        #open_cv_image = warped[:, :, ::-1]
        return warped

    def __sharpening_skimage(self, img, ratio):
        img = unsharp_mask(img, radius=ratio, amount=1)
        img = img_as_ubyte(img)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        return img

    def get_session(self):
        """ Construct a modified tf session.
        """
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True
        return tf.Session(config=config)

    def start(self,path_imagen):
        img = cv2.imread(path_imagen)
        cv2.imwrite("xxx.jpg",img)
        img_xxx = cv2.imread("xxx.jpg")
        img_proyec = self.__proyeccion(img_xxx)
        cv2.imwrite("xxx.jpg",img_proyec)
        img_proyec = cv2.imread("xxx.jpg")
        img_xxx = self.__sharpening_skimage(img_proyec,self.__ratio)
        cv2.imwrite("xxx.jpg",img_xxx)
        
        image_detections = deteccion_producto(self.model,"xxx.jpg")

        image_detections = image_detections

        X1 = image_detections['x1'].values.tolist()
        Y1 = image_detections['y1'].values.tolist()
        X2 = image_detections['x2'].values.tolist()
        Y2 = image_detections['y2'].values.tolist()

        BBOX = X1,Y1,X2,Y2

        with open(self.path_pickle_out, 'wb') as f:
            pickle.dump(BBOX, f)

        return img, BBOX
