
from Mask_R_CNN import mask_RCNN
import cv2

img = cv2.imread("48.png")
Mask_RCNN_Fleje = mask_RCNN("Fleje",  "Weights/annotations.json", "Weights/output", "Weights/model.pth")

prediction_fleje = Mask_RCNN_Fleje.get_prediction(img)

contorno = Mask_RCNN_Fleje.get_contour(prediction_fleje, "Fleje", 0.9)

img = cv2.polylines(img, contorno, True, (0,0,255),3)

cv2.imshow('Contours', img)

# Exiting the window if 'q' is pressed on the keyboard.
if cv2.waitKey(0) & 0xFF == ord('q'): 
    cv2.destroyAllWindows()