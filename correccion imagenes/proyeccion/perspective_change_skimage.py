import numpy as np
from skimage import transform
from skimage import io
import matplotlib.pyplot as plt
# construct the argument parse and parse the arguments
image = io.imread('./imagenes_originales/imagen_10.jpg')
text = image
dx = 2170-380
dy = 2830-700
src = np.array([[0, 0], [0, dy], [dx, dy], [dx, 0]])
dst = np.array([[380, 600], [480, 2830], [2020, 2600], [2170, 700]])

tform3 = transform.ProjectiveTransform()
tform3.estimate(src, dst)
warped = transform.warp(text, tform3, output_shape=(dy, dx))

fig, ax = plt.subplots(nrows=2, figsize=(8, 3))

ax[0].imshow(text, cmap=plt.cm.gray)
ax[0].plot(dst[:, 0], dst[:, 1], '.r')
ax[1].imshow(warped, cmap=plt.cm.gray)

for a in ax:
    a.axis('off')

plt.tight_layout()
plt.show()
