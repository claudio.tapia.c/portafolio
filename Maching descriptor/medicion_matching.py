import numpy as np
import cv2
import matplotlib.pyplot as plt
import time
import os
import imghdr
from pandas import ExcelWriter
from openpyxl import load_workbook
import pandas as pd

VP = 0
VN = 0
FP = 0
FN = 0
k_max = 0

ITERACIONES=20
OFFSET_ITERACION = 0.74
MIN_MATCH_COUNT = 10
FLANN_INDEX_KDTREE = 1

path_imagenes_planograma='/home/claudio/Escritorio/Matching descriptor/planograma_cremas'
lista_imagenes_planograma = os.listdir(path_imagenes_planograma)

path_imagenes_test = '/home/claudio/Escritorio/Matching descriptor/cortadas_gondola_cremas_manquehue'
lista_imagenes_test = os.listdir(path_imagenes_test)

archivo= "resultados_matching_1.xlsx"

if not os.path.isfile(archivo):
    df = pd.DataFrame({'Umbral': [],
                       'TP': [],
                       'TN': [],
                       'FP': [],
                       'FN': [],
                       'Precision': [],
                       'Exactitud': [],
                       'Recall': [],
                       'F1': [],
                       '1-Especificidad': []})
        
    writer = ExcelWriter(archivo)
    df.to_excel(writer, 'Hoja', index=False)
    writer.save()

# FLANN parameters
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 1)
search_params = dict(checks=1)   # or pass empty dictionary
flann = cv2.FlannBasedMatcher(index_params,search_params)
sift = cv2.xfeatures2d.SIFT_create()

for j in range(ITERACIONES):
    print("\n**********************************************************************")
    print("THRESHOLD: ", OFFSET_ITERACION + (1-OFFSET_ITERACION)*j/(ITERACIONES))

    for path_image_test in lista_imagenes_test:

        path_image_test_split = path_image_test.split("_")
        path_image_test_split = path_image_test_split[0] + "_" + path_image_test_split[1]+ ".jpg"

        img1 = cv2.imread(path_imagenes_test + "/" + path_image_test) 
        tic = time.time()
        img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
        kp1, des1 = sift.detectAndCompute(img1,None)

        for path_image_planograma in lista_imagenes_planograma:

            
            img2 = cv2.imread(path_imagenes_planograma + "/" + path_image_planograma) #trainImage
            img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
            # find the keypoints and descriptors with SIFT
            kp2, des2 = sift.detectAndCompute(img2,None)
            matches = flann.knnMatch(des1,des2,k=2)

            good = []
            dst_pts = []
            src_pts = []
            list_k_path = []
            lists_k_match_path = []

            # ratio test as per Lowe's paper
            for i,(m,n) in enumerate(matches):
                if m.distance < n.distance*(OFFSET_ITERACION + (1-OFFSET_ITERACION)*j/(ITERACIONES)):
                    good.append(m)
                    dst_pts.append(kp1[m.queryIdx].pt)
                    src_pts.append(kp2[m.trainIdx].pt)
            
            if len(good)>MIN_MATCH_COUNT:
                dst_pts= np.float32(dst_pts).reshape(-1,1,2)
                src_pts= np.float32(src_pts).reshape(-1,1,2)
                M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
                matchesMask_ = mask.ravel().tolist()

                k=0

                for e in matchesMask_:
                    if e==1:
                        k=k+1

                dict_match_path = {"path image planograma": path_image_planograma, "match":k}
                list_k_path.append(dict_match_path)

                

                if k_max<k:
                    k_max=k
                    image_match=img2 
                    kp2_match=kp2
                    des2_match=des2
                    matches_m=good
                    matchesMask_m=matchesMask_
                    path_image_match = path_image_planograma     
                    k_max_anterior=k_max
            else:
                #print( "\n Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT))
                matchesMask_ = None
        
        #list_k_path = sorted(list_k_path, key = lambda i: i["match"],reverse=True)
        #dict_k_match_path = {"path image test": path_image_planograma, "ranking":list_k_path}
        #lists_k_match_path = lists_k_match_path.append(dict_k_match_path)
        if k_max==0:

            if path_image_test_split in lista_imagenes_planograma:

                FN+=1
                #print("VN actual: ", VN)
            else:
                VN+=1
                #print("FN actual: ", FN)

        else:
            print("\nImagen test: ", path_image_test_split)
            print("*IMAGEN match: {}   num match: {} \n".format(path_image_match,k_max))
            
            if path_image_match == path_image_test_split:

                VP+=1
                #print("VP actual: ", VP)
                print("*Path match: {}   num match: {} \n".format(path_image_match,k_max))
            else:
                FP+=1
                #print("FP actual: ", FP)

        k_max = 0

    if VP == 0:
        precision = 0
        sensibilidad = 0
        recall = 0
    else:
        precision = 100*VP/(VP+FP)
        recall = 100*VP/(VP+FN)

    if (VP+VN) == 0:
        exactitud = 0
    else:
        exactitud = 100*(VP+VN)/(VP+FN+FP+VN)
    
    if precision*recall == 0:
        f1 = 0
    else:
        f1 = (2*precision*recall)/(precision + recall)

    if VN == 0:
        especificidad = 100
    else:
        especificidad = 100-100*VN/(VN+FP)

    doc=load_workbook(archivo)
    hoja = doc.get_sheet_by_name(doc.get_sheet_names()[0])

    hoja.cell(row = j + 2, column = 1).value = (j/ITERACIONES)
    hoja.cell(row = j + 2, column = 2).value = VP
    hoja.cell(row = j + 2, column = 3).value = VN
    hoja.cell(row = j + 2, column = 4).value = FP
    hoja.cell(row = j + 2, column = 5).value = FN
    hoja.cell(row = j + 2, column = 6).value = precision
    hoja.cell(row = j + 2, column = 7).value = exactitud
    hoja.cell(row = j + 2, column = 8).value = recall
    hoja.cell(row = j + 2, column = 9).value = f1
    hoja.cell(row = j + 2, column = 10).value = especificidad
    doc.save(archivo)
    
    #print("cantidad de imagenes: " + str(cant_im))
    print("Verdaderos positivos: " + str(VP))
    print("Falsos positivos: " + str(FP) )
    print("Falsos negativos: " + str(FN))
    print("Verdaderos negativos: " + str(VN))
    print("\n")
    print("1-Especificidad: " + str(especificidad)+"%")
    print("Recall: " + str(recall)+"%")
    print("Precision: " + str(precision)+"%")
    print("Exactitud: " + str(exactitud)+"%")
    print("F1: " + str(f1)+"%")
    print("THRESHOLD: ", OFFSET_ITERACION + (1-OFFSET_ITERACION)*j/(ITERACIONES))
    print("**********************************************************************")
    print("\n \n")

    VP = 0
    VN = 0
    FP = 0
    FN = 0
   