import numpy as np
import cv2
import matplotlib.pyplot as plt
import time
import os
import imghdr
import lpips
from PIL import Image
from torchvision import transforms


def normalizar_imagen(img):
    img = 2*((img- img.min()) / (img.max() - img.min()))-1
    return img


def cv2_to_pytorch(img):
    img = Image.fromarray(img).convert('RGB')  #img as opencv
    tensor_img = transforms.ToTensor()(img).unsqueeze_(0)
    return tensor_img


def squeare_image(img):
    size = img.shape
    
    h = size[0]
    w = size[1]
    d = size[2]
    
    sobra = int((abs(w-h))/2)
    promedio = int((w+h)/2)
    
    if w > h:
        
        img_cuadrada = np.zeros((w, w, d))
        img_cuadrada[sobra:promedio,:, :] = img
      
    else:
        img_cuadrada = np.zeros((h, h, d))
        img_cuadrada[:,sobra:promedio, :] = img
        
    img_cuadrada = cv2.resize(img_cuadrada, (1000,1000), interpolation = cv2.INTER_AREA)
    
    return img_cuadrada.astype(np.uint8)


class match_lpips():

    def __init__(self):
        
        self.path_imagenes='/home/claudio/Escritorio/Matching descriptor/planograma_bucal'
        self.lista_imagenes = os.listdir(self.path_imagenes)

        self.loss_fn = lpips.LPIPS(net='alex') # best forward scores

        # FLANN parameters
        
        FLANN_INDEX_KDTREE = 0
        index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
        search_params = dict(checks=50)   # or pass empty dictionary
        self.flann = cv2.FlannBasedMatcher(index_params,search_params)
        self.sift = cv2.xfeatures2d.SIFT_create()

    def start(self,img):
        MIN_MATCH_COUNT = 1
        dist_min = 2
        ratio=0.745
        kp1, des1 = self.sift.detectAndCompute(img,None)
        x = squeare_image(img)
        x = cv2_to_pytorch(x)
        tensor_img = normalizar_imagen(x)
        
        for path_image in self.lista_imagenes:

            img2 = cv2.imread(self.path_imagenes + "/" + path_image) #trainImage
            
            # find the keypoints and descriptors with SIFT
            kp2, des2 = self.sift.detectAndCompute(img2,None)
            matches = self.flann.knnMatch(des1,des2,k=2)

            good = []
            dst_pts = []
            src_pts = []
            k = 0

            for i,(m,n) in enumerate(matches):
                if m.distance < ratio*n.distance:
                    good.append(m)
                    k+=1

            if k >= MIN_MATCH_COUNT:

                try:
                
                    # h,w,d = img2.shape
                    # pts = np.float32([[0, 0], [0, h-1], [w-1, h-1], [w-1, 0]]).reshape(-1, 1, 2) 
                    # src_pts = np.float32([ kp1[m.queryIdx].pt for m in good ]).reshape(-1,1,2)
                    # dst_pts = np.float32([ kp2[m.trainIdx].pt for m in good ]).reshape(-1,1,2)
                    # M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
                    # dst = cv2.perspectiveTransform(pts,M)
                    # perspectiveM = cv2.getPerspectiveTransform(np.float32(dst),pts)
                    # found = cv2.warpPerspective(img,perspectiveM,(w,h))
                    
                    # img_copy = img.copy()

                    # img_poly = cv2.polylines(img_copy,[np.int32(dst)],True,255,3, cv2.LINE_AA)

                    bf = cv2.BFMatcher(cv2.NORM_L1,crossCheck=False)

                    matches = bf.match(des1,des2)
                    k = len(matches)
                    x = list()
                    y= list()

                    for m in matches:
                        (x1, y1) = kp2[m.trainIdx].pt
                        x.append(x1)
                        y.append(y1)
                    
                    x.sort()
                    y.sort()

                    x_start = int(x[0])
                    x_end = int(x[-1])
                    y_start = int(y[0])
                    y_end = int(y[-1])

                    cropped = img[x_start:x_end, y_start:y_end]

                    x = squeare_image(img2)
                    x = cv2_to_pytorch(x)
                    tensor_img2 = normalizar_imagen(x)

                    x = squeare_image(cropped)
                    cv2.imwrite("xxx.jpg", x)
                    x = cv2_to_pytorch(x)
                    tensor_img = normalizar_imagen(x)

                    dist = self.loss_fn(tensor_img, tensor_img2)
                    dist = dist.item()
                    
                    print(k)

                    if dist<dist_min:
                        dist_min = dist
                        image_match=img2 
                        path_image_m = path_image
                        cant_key_m= k
                except:
                    pass

        if dist_min != 2:
            
            print(dist_min)
            print(cant_key_m)
            print(path_image_m)
            fig, axes = plt.subplots(1, 2)
            fig.add_subplot(1, 2, 1)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            plt.imshow(img)
            fig.add_subplot(1, 2, 2)
            image_match = cv2.cvtColor(image_match, cv2.COLOR_BGR2RGB)
            plt.imshow(image_match)
            fig.savefig('figures/' + path_image_m)
            dist_min = 2
