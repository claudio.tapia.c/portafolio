import pandas as pd
import os
from numba import njit, jit, prange
import numpy as np

path_imagenes_malas = "./imagenes_malas/"
path_annotations_csv = "./annotations/annotations_train.csv"
df = pd.read_csv(path_annotations_csv,header=None)
lista_imagenes_malas = os.listdir(path_imagenes_malas)

@njit
def indices_(lista_imagenes_malas,imagenes_annotations):
    indices = []
    len_imagenes_malas = len(lista_imagenes_malas)
    len_imagen_annotations = len(imagenes_annotations)
    
    for index_imagen_mala in prange(len_imagenes_malas):
        for index_annotations in prange(len_imagen_annotations):
            if imagenes_annotations[index_annotations]==lista_imagenes_malas[index_imagen_mala]:
                indices.append(index_annotations)
    
    return indices

imagenes_annotations = df[0].values.tolist()
X1 = df[1].values.tolist()
Y1 = df[2].values.tolist()
X2 = df[3].values.tolist()
Y2 = df[4].values.tolist()
tipo = df[5].values.tolist()
height = df[6].values.tolist()
width = df[7].values.tolist()

index = indices_(lista_imagenes_malas,imagenes_annotations)

imagenes_annotations = np.delete(imagenes_annotations,index).tolist()
X1 = np.delete(X1,index).tolist()
Y1 = np.delete(Y1,index).tolist() 
X2 = np.delete(X2,index).tolist() 
Y2 = np.delete(Y2,index).tolist() 
tipo = np.delete(tipo,index).tolist() 
height = np.delete(height,index).tolist() 
width = np.delete(width,index).tolist() 

df2 = pd.DataFrame([*zip(imagenes_annotations,X1,Y1,X2,Y2,tipo,height,width)])

df2.to_csv('annotations_train.csv', header=False, index=False)
