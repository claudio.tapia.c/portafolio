#!/usr/bin/env python

import argparse
import os
import sys

import keras
import tensorflow as tf
import cv2

from object_detector_retinanet.keras_retinanet import models
from object_detector_retinanet.keras_retinanet.preprocessing.csv_generator import CSVGenerator
from object_detector_retinanet.keras_retinanet.utils.predict_iou import deteccion_producto
from object_detector_retinanet.keras_retinanet.utils.keras_version import check_keras_version
from object_detector_retinanet.utils import image_path, annotation_path, root_dir
#from object_detector_retinanet.keras_retinanet.matching import matching_sift

def get_session():
    """ Construct a modified tf session.
    """
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)


def main(args=None):
    # parse arguments
    model_path = "/home/claudio/Documentos/SKU110K/snapshots/iou_resnet50_csv_06.h5"
    path_imagenes ="/home/claudio/Documentos/SKU110K/images/"
    lista_imagenes = os.listdir(path_imagenes)
    # Blue color in BGR 
    color = (255, 0, 0) 
    # Line thickness of 2 px 
    thickness = 3

    use_cpu = False

    gpu_num = str(0)

    if use_cpu:
        os.environ["CUDA_VISIBLE_DEVICES"] = str(666)
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = gpu_num
    tf.compat.v1.keras.backend.get_session()

    # load the model
    model = models.load_model(os.path.join(root_dir(), model_path), backbone_name='resnet50', convert=1, nms=False)

    for path_imagen in lista_imagenes:
        img = cv2.imread(path_imagenes + path_imagen ) 
        # start prediction
        image_detections = deteccion_producto(model,img)

        for image_box in image_detections:

            x1 = int(image_box[0])
            y1 = int(image_box[1])
            x2 = int(image_box[2])
            y2 = int(image_box[3])
            
            start_point = (x1,y1)
            end_point = (x2,y2)
            img_crop = img[y1:y2+1, x1:x2+1]
            cv2.imwrite("a.jpg", img_crop)
            #matching_sift.match_sift(img_crop)
            img = cv2.rectangle(img, start_point, end_point, color, thickness)
        
        cv2.imwrite(path_imagen, img)


if __name__ == '__main__':
    main()
