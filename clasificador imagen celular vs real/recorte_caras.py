import cv2
import os
from random import randint


image_size = 160


cascPath = './cv2/haarcascade_frontalface_alt2.xml'

images_reales_Path='./caras_reales/'
fotos_Path='./fotos2/'

fotos_filepaths = [os.path.join(fotos_Path, f) for f in os.listdir(fotos_Path)]
acum=0

for f in fotos_filepaths:

    frame = cv2.imread(f) 
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    face_cascade = cv2.CascadeClassifier(cascPath)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5,minSize=(120, 120))
    

    for (x,y,w,h) in faces:
        roi_gray = gray[y:y+h, x:x+w]
        roi_color = frame[y:y+h, x:x+w]
        roi_color=cv2.resize(roi_color,(image_size,image_size))
        cv2.imwrite(images_reales_Path + str(randint(0,99999999)) +"holaa" +".jpg",roi_color)


