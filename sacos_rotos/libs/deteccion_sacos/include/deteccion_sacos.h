#include "opencv2/opencv.hpp"
#include "opencv2/core/cuda.hpp"
#include <iostream>
#include <string>
#include <torch/csrc/autograd/grad_mode.h>
#include <torch/csrc/jit/runtime/graph_executor.h>
#include <torch/script.h>

#include <cstdlib>

using namespace cv;
using namespace std;

//torch::jit::script::Module cargarmodelo(string model_path);

c10::IValue get_caffe2_tracing_inputs(cv::Mat& img, c10::Device device);

c10::IValue get_tracing_inputs(cv::Mat& img, c10::Device device);

c10::IValue get_scripting_inputs(cv::Mat& img, c10::Device device);

c10::IValue get_inputs(string export_method, cv::Mat& img, c10::Device device);

struct MaskRCNNOutputs 
{
    at::Tensor pred_boxes, pred_classes, pred_masks, scores;
    int num_instances() const {
        return pred_boxes.sizes()[0];
    }
};

MaskRCNNOutputs get_outputs(string export_method, c10::IValue outputs);