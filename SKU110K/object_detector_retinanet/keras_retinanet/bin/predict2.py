#!/usr/bin/env python

import argparse
import os
import sys

import keras
import tensorflow as tf
import cv2
import numpy as np
from skimage.transform import hough_line, hough_line_peaks
from skimage import transform
import imutils
from skimage.filters import unsharp_mask
from skimage import img_as_ubyte

import pickle

from object_detector_retinanet.keras_retinanet import models
from object_detector_retinanet.keras_retinanet.preprocessing.csv_generator import CSVGenerator
from object_detector_retinanet.keras_retinanet.utils.predict_iou import deteccion_producto
from object_detector_retinanet.keras_retinanet.utils.keras_version import check_keras_version
from object_detector_retinanet.utils import image_path, annotation_path, root_dir
from object_detector_retinanet.keras_retinanet.matching import matching_lpips



def proyeccion(img):
    num_rows, num_cols = img.shape[:2]
    img_original = img.copy()
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    img = imutils.auto_canny(img)
    tested_angles = np.linspace(-np.pi / 2, np.pi / 2, 360, endpoint=False)
    h, theta, d = hough_line(img)
    y_0 = []
    y_1 = []
    origin = np.array((0, img.shape[1]))
    origin_x = np.array((0, img.shape[0]))
    for _, angle, dist in zip(*hough_line_peaks(h, theta, d, min_distance=100, min_angle=70)):
        y0, y1 = (dist - origin * np.cos(angle)) / np.sin(angle)
        #horizontales
        if y0>0 and y0<num_rows and y1>0 and y1<num_rows and abs(y1-y0)<500:
            y_0.append(int(y0))
            y_1.append(int(y1))

    p14_i = y_0.index(min(y_0))
    p23_i = y_0.index(max(y_0))
    p1 = [0, y_0[p14_i]]
    p2 = [0, y_0[p23_i]]
    p3 = [num_cols-1, y_1[p23_i]]
    p4 = [num_cols-1, y_1[p14_i]]
    y_ref_up = min(p1[1],p4[1])
    y_ref_down = min(p2[1],p3[1])
    src = np.array([[0, y_ref_up], [0, y_ref_down], [num_cols-1, y_ref_down], [num_cols-1, y_ref_up]])
    dst = np.array([p1,p2,p3,p4])

    tform3 = transform.ProjectiveTransform()
    tform3.estimate(src, dst)
    warped = transform.warp(img_original, tform3, output_shape=(num_rows, num_cols))
    warped = img_as_ubyte(warped)
    #open_cv_image = warped[:, :, ::-1]
    return warped

def sharpening_skimage(img, ratio):
    img = unsharp_mask(img, radius=ratio, amount=1)
    img = img_as_ubyte(img)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    return img


def get_session():
    """ Construct a modified tf session.
    """
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)


def main(args=None):
    # parse arguments
    model_path = "/home/claudio/Documentos/SKU110K/snapshots/iou_resnet50_csv_01.h5"
    path_imagenes ="/home/claudio/Documentos/SKU110K/imagenes gondolas/"
    path_imagenes_cortadas ="/home/claudio/Documentos/SKU110K/imagenes_cortadas/"

    lista_path_dir = os.listdir(path_imagenes)
    print(lista_path_dir)
    # Blue color in BGR 
    color = (255, 0, 0) 
    # Line thickness of 2 px 
    thickness = 3

    use_cpu = False

    gpu_num = str(0)

    if use_cpu:
        os.environ["CUDA_VISIBLE_DEVICES"] = str(666)
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = gpu_num
    tf.compat.v1.keras.backend.get_session()

    # load the model
    model = models.load_model(os.path.join(root_dir(), model_path), backbone_name='resnet50', convert=1, nms=False)
    match = matching_lpips.match_lpips()
    
    
    i = 0
    for path_imagen in lista_path_dir:
        
        e = 0
        

        i+=1
        name_split = path_imagen.split(".jpg")[0]
        print(path_imagenes +  path_imagen)
        img = cv2.imread(path_imagenes + path_imagen)
        cv2.imwrite("xxx.jpg",img)
        img_xxx = cv2.imread("xxx.jpg")
        img_proyec = proyeccion(img_xxx)
        cv2.imwrite("xxx.jpg",img_proyec)
        img_proyec = cv2.imread("xxx.jpg")
        img_copy = img_proyec.copy()
        img_xxx = sharpening_skimage(img_proyec,1.5)
        cv2.imwrite("xxx.jpg",img_xxx)
        # start prediction
        image_detections = deteccion_producto(model,"xxx.jpg")

        with open('bbox.pickle', 'wb') as f:
            pickle.dump(image_detections, f)

        try:
            os.stat(path_imagenes_cortadas + name_split + "/")
        except:
            os.mkdir(path_imagenes_cortadas + name_split + "/")

        
        cv2.imwrite(path_imagenes_cortadas + name_split+ "/" + path_imagen, img_copy)

        for x1,y1,x2,y2 in zip(image_detections['x1'], image_detections['y1'], image_detections['x2'], image_detections['y2']):
            e+=1

            try:
                os.stat(path_imagenes_cortadas + name_split +  "/cortadas/")
            except:
                os.mkdir(path_imagenes_cortadas + name_split  + "/cortadas/")

            #x1 = int(image_box[0])
            #y1 = int(image_box[1])
            #x2 = int(image_box[2])
            #y2 = int(image_box[3])

            size = img_copy.shape
    
            h = size[0]
            w = size[1]
            d = size[2]

            offset_x = int(w/70)
            offset_y = int(h/33)
            
            #offset_x = 0
            #offset_y = 0
            y1_arriba = y1-offset_y
            #y2_abajo = y2+1+offset_y
            y2_abajo = y2+1
            x1_izquierda = x1-offset_x
            x2_derecha = x2+1+offset_x

            if y1_arriba < 0:
                y1_arriba = 0

            if x1_izquierda < 0:
                x1_izquierda = 0
            
            if y2_abajo > h:
                y2_abajo = h

            if x2_derecha > w:
                x2_derecha = w

            start_point = (x1,y1)
            end_point = (x2,y2)
            img_crop = img_copy[y1_arriba:y2_abajo, x1_izquierda:x2_derecha]


            cv2.imwrite(path_imagenes_cortadas + name_split +  "/cortadas/" + str(e) + ".jpg" , img_crop)
            #match.start(img_crop)
            
            img_proyec = cv2.rectangle(img_proyec, start_point, end_point, color, thickness)

            
        cv2.imwrite(path_imagenes_cortadas + name_split + "/detecciones_.jpg" , img_proyec)


if __name__ == '__main__':
    main()
