import pandas as pd
import os
from numba import njit, jit, prange
import numpy as np
from functools import partial

path_imagenes_malas = "./imagenes_malas/"
path_annotations_csv = "./annotations/annotations_train.csv"


@njit
def indices_(lista_imagenes_malas,imagenes_annotations):
    indices = []
    len_imagenes_malas = len(lista_imagenes_malas)
    len_imagen_annotations = len(imagenes_annotations)
    
    for index_imagen_mala in prange(len_imagenes_malas):
        for index_annotations in prange(len_imagen_annotations):
            if imagenes_annotations[index_annotations]==lista_imagenes_malas[index_imagen_mala]:
                indices.append(index_annotations)
    
    return indices

def partition(lst, n): 
    return [ lst[i::n] for i in range(n) ]

def duplicates(lst, item):
    return [i for i, x in enumerate(lst) if x == item]

df = pd.read_csv(path_annotations_csv,header=None)



imagenes_annotations_filter = list(set(df[0].tolist()))

lista_dividida = partition(imagenes_annotations_filter, 4)

imagenes_annotations = df[0].values.tolist()
X1 = df[1].values.tolist()
Y1 = df[2].values.tolist()
X2 = df[3].values.tolist()
Y2 = df[4].values.tolist()
tipo = df[5].values.tolist()
height = df[6].values.tolist()
width = df[7].values.tolist()

for i, list_images in enumerate(lista_dividida):
    print(len(list_images))
    imagenes_annotations_part = []
    X1_part = []
    Y1_part = []
    X2_part = []
    Y2_part = []
    tipo_part = []
    height_part = []
    width_part = []

    for images in list_images:
        indexs = duplicates(imagenes_annotations,images)
        
        for index in indexs:
            imagenes_annotations_part.append(imagenes_annotations[index])
            X1_part.append(X1[index])
            Y1_part.append(Y1[index])
            X2_part.append(X2[index])
            Y2_part.append(Y2[index])
            tipo_part.append(tipo[index])
            height_part.append(height[index])
            width_part.append(width[index])

    df2 = pd.DataFrame([*zip(imagenes_annotations_part,X1_part,Y1_part,X2_part,Y2_part,tipo_part,height_part,width_part)])

    path_annotations = 'annotations_train_part_' + str(i+1) + '.csv'
    df2.to_csv(path_annotations, header=False, index=False)