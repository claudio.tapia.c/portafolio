#! /usr/bin/env python
# -*- coding: utf-8 -*-

import os
import tensorflow as tf
import cv2
import csv

def main(args=None):
    # parse arguments
    path_images = "/home/claudio/Documentos/SKU110K/images/"
    path_imagenes_etiquetas ="../OK/"
    path_train_annotations ="../annotations/annotations_train.csv"
    lista_imagenes_etiquetas = os.listdir(path_imagenes_etiquetas)
    lista_imagenes = list(filter(lambda x: ('.txt' not in x), lista_imagenes_etiquetas))
    lista_etiquetas = list(filter(lambda x: ('.jpg' not in x), lista_imagenes_etiquetas))  
    lista_etiquetas = list(filter(lambda x: ('classes.txt' not in x), lista_etiquetas)) 
    i = 0
    with open(path_train_annotations, mode='w') as employee_file:
        employee_writer = csv.writer(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        for path_imagen, path_etiqueta in zip(lista_imagenes,lista_etiquetas):

            i+=1

            img = cv2.imread(path_imagenes_etiquetas + path_imagen)
            cv2.imwrite("../xxx.jpg",img)
            img_copy=img.copy()
            
            size = img_copy.shape
        
            h = size[0]
            w = size[1]

        
            with open(path_imagenes_etiquetas + path_etiqueta, 'r') as f:
                for line in f:

                    xc_bbox = line.split(' ')[1]
                    yc_bbox = line.split(' ')[2]
                    w_bbox = line.split(' ')[3]
                    h_bbox = line.split(' ')[4]

                    x1 = int(w*(float(xc_bbox) - float(w_bbox)/2))
                    y1 = int(h*(float(yc_bbox) - float(h_bbox)/2))
                    x2 = int(w*(float(xc_bbox) + float(w_bbox)/2))
                    y2 = int(h*(float(yc_bbox) + float(h_bbox)/2))
                    
                    employee_writer.writerow([path_images + path_imagen, x1, y1, x2, y2,"other", w, h])



if __name__ == '__main__':
    main()
