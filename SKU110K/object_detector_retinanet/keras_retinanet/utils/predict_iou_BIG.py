"""
Copyright 2017-2018 Fizyr (https://fizyr.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from __future__ import print_function

import csv

import datetime

from .EmMerger import *
from object_detector_retinanet.utils import create_folder, root_dir
from .visualization import draw_detections, draw_annotations

import numpy as np
import os

import cv2
import pandas
from ..utils.image import (
    TransformParameters,
    adjust_transform_for_image,
    apply_transform,
    preprocess_image,
    resize_image,
)

def non_max_suppression_fast(boxes, overlapThresh):
   # if there are no boxes, return an empty list
   if len(boxes) == 0:
      return []

   # if the bounding boxes integers, convert them to floats --
   # this is important since we'll be doing a bunch of divisions
   if boxes.dtype.kind == "i":
      boxes = boxes.astype("float")
#  
   # initialize the list of picked indexes   
   pick = []

   # grab the coordinates of the bounding boxes
   x1 = boxes[:,0]
   y1 = boxes[:,1]
   x2 = boxes[:,2]
   y2 = boxes[:,3]

   # compute the area of the bounding boxes and sort the bounding
   # boxes by the bottom-right y-coordinate of the bounding box
   area = (x2 - x1 + 1) * (y2 - y1 + 1)
   idxs = np.argsort(y2)

   # keep looping while some indexes still remain in the indexes
   # list
   while len(idxs) > 0:
      # grab the last index in the indexes list and add the
      # index value to the list of picked indexes
      last = len(idxs) - 1
      i = idxs[last]
      pick.append(i)

      # find the largest (x, y) coordinates for the start of
      # the bounding box and the smallest (x, y) coordinates
      # for the end of the bounding box
      xx1 = np.maximum(x1[i], x1[idxs[:last]])
      yy1 = np.maximum(y1[i], y1[idxs[:last]])
      xx2 = np.minimum(x2[i], x2[idxs[:last]])
      yy2 = np.minimum(y2[i], y2[idxs[:last]])

      # compute the width and height of the bounding box
      w = np.maximum(0, xx2 - xx1 + 1)
      h = np.maximum(0, yy2 - yy1 + 1)

      # compute the ratio of overlap
      overlap = (w * h) / area[idxs[:last]]

      # delete all indexes from the index list that have
      idxs = np.delete(idxs, np.concatenate(([last],
         np.where(overlap > overlapThresh)[0])))

   # return only the bounding boxes that were picked using the
   # integer data type
   return boxes[pick].astype("int")


def deteccion_producto(
        model,
        path_img):

    kernel_dim = 3
    
    max_detections=9999
    
    hard_score_rate = 0.9
    score_threshold=0
    
    image = cv2.imread(path_img)

    size = image.shape

    h = size[0]
    w = size[1]

    cant_trozos_x = int(w/kernel_dim)
    cant_trozos_y = int(h/kernel_dim)
    matrix_count = np.zeros((cant_trozos_y,cant_trozos_x))
    matrix_true = np.zeros((cant_trozos_y,cant_trozos_x))

    y_max = np.arange(1,h+1)
    x_max = np.arange(1,w+1)
    image_r = image[:,:,0]
   
    print(matrix_count.shape)

    #image = preprocess_image(img.copy())
    image, scale = resize_image(image)

    
    # run network
    boxes, hard_scores, labels, soft_scores = model.predict_on_batch(np.expand_dims(image, axis=0))
    soft_scores = np.squeeze(soft_scores, axis=-1)
    soft_scores = hard_score_rate * hard_scores + (1 - hard_score_rate) * soft_scores
    # correct boxes for image scale
    boxes /= scale

    # select indices which have a score above the threshold
    indices = np.where(hard_scores[0, :] > score_threshold)[0]

    # select those scores
    scores = soft_scores[0][indices]
    hard_scores = hard_scores[0][indices]

    # find the order with which to sort the scores
    scores_sort = np.argsort(-scores)[:max_detections]

    # select detections
    image_boxes = boxes[0, indices[scores_sort], :]
    image_scores = scores[scores_sort]
    image_hard_scores = hard_scores[scores_sort]
    image_labels = labels[0, indices[scores_sort]]

    image_detections = np.concatenate(
        [image_boxes, np.expand_dims(image_scores, axis=1), np.expand_dims(image_labels, axis=1)], axis=1)
    results = np.concatenate(
        [image_boxes, np.expand_dims(image_scores, axis=1), np.expand_dims(image_hard_scores, axis=1),
            np.expand_dims(image_labels, axis=1)], axis=1)

    result_df = pandas.DataFrame()
    result_BIG = pandas.DataFrame()
    result_df['x1'] = results[:, 0].astype(int)
    result_df['y1'] = results[:, 1].astype(int)
    result_df['x2'] = results[:, 2].astype(int)
    result_df['y2'] = results[:, 3].astype(int)

    print(x_max)
    X1 = []
    Y1 = []
    X2 = []
    Y2 = []

    for x1,y1,x2,y2 in zip(result_df['x1'], result_df['y1'], result_df['x2'], result_df['y2']):
        for trozo_x in range(cant_trozos_x):
            for trozo_y in range(cant_trozos_y):
                kernel_x1 = trozo_x*kernel_dim
                kernel_x2 = (trozo_x+1)*kernel_dim
                kernel_y1 = trozo_y*kernel_dim
                kernel_y2 = (trozo_y+1)*kernel_dim

                if kernel_x1 >=x1 and kernel_x2<=x2 and kernel_y1>=y1 and kernel_y2<=y2:
                    matrix_count[trozo_y, trozo_x] +=1 
                    if matrix_count[trozo_y, trozo_x] > 30:
                        X1.append(x1)
                        Y1.append(Y1)
                        X2.append(x2)
                        Y2.append(y2)
                        matrix_true[trozo_y, trozo_x] = 1
    
    result_BIG['x1'] = X1
    result_BIG['y1'] = Y1
    result_BIG['x2'] = X2
    result_BIG['y2'] = Y2

    print(result_BIG['x1'] )
    #image_detections = image_detections.to_dict()
 
    #image_detections = non_max_suppression_fast(image_detections, 0*0.1618033988749)
    
    return result_BIG