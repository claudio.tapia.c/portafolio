import numpy as np
import cv2
import matplotlib.pyplot as plt
import time
import os
import imghdr
from pandas import ExcelWriter
from openpyxl import load_workbook
import pandas as pd
import json
from numpy import linalg as LA

VP = 0
VN = 0
FP = 0
FN = 0

ratio=0.745

k_max = 0

TOP = 10

MIN_MATCH_COUNT = 10
FLANN_INDEX_KDTREE = 1

path_imagenes_planograma='/home/claudio/Escritorio/Matching descriptor/planograma prueba'
lista_imagenes_planograma = os.listdir(path_imagenes_planograma)

path_imagenes_test = '/home/claudio/Escritorio/Matching descriptor/test prueba'
lista_imagenes_test = os.listdir(path_imagenes_test)
print(lista_imagenes_test)
archivo= "resultados_matching_sin_iteracion_2.xlsx"

if not os.path.isfile(archivo):
    df = pd.DataFrame({'Producto': [],
                       'Ranking': []})
        
    writer = ExcelWriter(archivo)
    df.to_excel(writer, 'Hoja', index=False)
    writer.save()

# FLANN parameters
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 1)
search_params = dict(checks=1)   # or pass empty dictionary
flann = cv2.FlannBasedMatcher(index_params,search_params)
sift = cv2.xfeatures2d.SIFT_create()

print("\n**********************************************************************")

lists_k_match_path = []

def distancia_interseccion_histograma(hist1,hist2):
    size = hist1.size
    s=0
    print(size)
    for i in range(size):
        d=sum(min(hist1[i],hist2[i]))
        s+=d
    return s

for path_image_test in lista_imagenes_test:

    path_image_test_split = path_image_test.split("_")
    path_image_test_split = path_image_test_split[0] + "_" + path_image_test_split[1]+ ".jpg"

    img1 = cv2.imread(path_imagenes_test + "/" + path_image_test) 
    tic = time.time()
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
    kp1, des1 = sift.detectAndCompute(img1,None)

    img1_r = img1[:,:,0]
    img1_g = img1[:,:,1]
    img1_b = img1[:,:,2]

    img1_r= 255*((img1_r-img1_r.min())/(img1_r.max()-img1_r.min()))
    img1_g= 255*((img1_g-img1_g.min())/(img1_g.max()-img1_g.min()))
    img1_b= 255*((img1_b-img1_b.min())/(img1_b.max()-img1_b.min()))

    img1[:,:,0] = img1_r.astype(int)
    img1[:,:,1] = img1_g.astype(int)
    img1[:,:,2] = img1_b.astype(int)

    list_k_path = []
    hist1 = []
    color = ('r','g','b')
    
    for i,col in enumerate(color):
        hist1.append(cv2.calcHist([img1],[i],None,[128],[0,256]))

    hist1 = np.concatenate((hist1[0], hist1[1], hist1[2]), axis=None)

    hist1=hist1/LA.norm(hist1)
    
    for path_image_planograma in lista_imagenes_planograma:

        img2 = cv2.imread(path_imagenes_planograma + "/" + path_image_planograma) #trainImage
        img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
        img2_r = img2[:,:,0]
        img2_g = img2[:,:,1]
        img2_b = img2[:,:,2]

        img2_r= 255*((img2_r-img2_r.min())/(img2_r.max()-img2_r.min()))
        img2_g= 255*((img2_g-img2_g.min())/(img2_g.max()-img2_g.min()))
        img2_b= 255*((img2_b-img2_b.min())/(img2_b.max()-img2_b.min()))

        img2[:,:,0] = img2_r.astype(int)
        img2[:,:,1] = img2_g.astype(int)
        img2[:,:,2] = img2_b.astype(int)

        # find the keypoints and descriptors with SIFT
        kp2, des2 = sift.detectAndCompute(img2,None)
        matches = flann.knnMatch(des1,des2,k=2)

        hist2 = []

        for i,col in enumerate(color):
            hist2.append(cv2.calcHist([img2],[i],None,[128],[0,256]))

        hist2 = np.concatenate((hist2[0], hist2[1], hist2[2]), axis=None)

        hist2=hist2/LA.norm(hist2)

        dist = cv2.compareHist(hist1,hist2,cv2.HISTCMP_CORREL)

        good = []
        dst_pts = []
        src_pts = []
        acum_distancia = 0
        dict_match_path = {}

        # ratio test as per Lowe's paper
        
        for i,(m,n) in enumerate(matches):
            if m.distance < n.distance*ratio:
            
                acum_distancia = acum_distancia + m.distance
                good.append(m)
                dst_pts.append(kp1[m.queryIdx].pt)
                src_pts.append(kp2[m.trainIdx].pt)
        
        if len(good) > MIN_MATCH_COUNT:
            dst_pts= np.float32(dst_pts).reshape(-1,1,2)
            src_pts= np.float32(src_pts).reshape(-1,1,2)
            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
            matchesMask_ = mask.ravel().tolist()
            #print(M)
            k=0
    
            for e in matchesMask_:
                if e==1:
                    k=k+1
            
            dict_match_path = dict(Imagen_planograma = path_image_planograma, Match = k)

            list_k_path.append(dict_match_path)
            
            if k_max < k:
                k_max = k
                image_match = img2 
                kp2_match = kp2
                des2_match = des2
                matches_m = good
                matchesMask_m = matchesMask_
                path_image_match = path_image_planograma   
                acum_distancia_anterior = acum_distancia
                
        else:
            #print( "\n Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT))
            matchesMask_ = None
    
    if k_max==0:
            
        if path_image_test_split in lista_imagenes_planograma:

            FN+=1
            #print("VN actual: ", VN)
        else:
            VN+=1
            #print("FN actual: ", FN)

    else:
        print("\nImagen test: ", path_image_test)
        print("imagen match: {}   num match: {} \n".format(path_image_match,k_max))

        if path_image_match == path_image_test_split:

            VP+=1
            #print("VP actual: ", VP)
            print("MATCH EXITOSO!!!!!!\n")
        else:
            FP+=1
            #Ordena de mayor a menor match
            list_k_path = sorted(list_k_path, key = lambda i: i["Match"],reverse=True)

            #hace el recorte de los mejores puntajes
            list_k_path = list_k_path[0:TOP]

            ranking = [lista["Imagen_planograma"] for lista in list_k_path]
            print(ranking)
            try:
                pos = ranking.index(path_image_test_split) + 1
                print("La posicion es", pos)
            except:
                pos = "NO"

            dict_k_match_path = {"Path image test": path_image_test,"Pos":pos, "Ranking":list_k_path[0:TOP]}
    
            lists_k_match_path.append(dict_k_match_path)
            #print(lists_k_match_path)
            #print("FP actual: ", FP)

    k_max = 0

if VP == 0:
    precision = 0
    sensibilidad = 0
    recall = 0
else:
    precision = 100*VP/(VP+FP)
    recall = 100*VP/(VP+FN)

if (VP+VN) == 0:
    exactitud = 0
else:
    exactitud = 100*(VP+VN)/(VP+FN+FP+VN)

if precision*recall == 0:
    f1 = 0
else:
    f1 = (2*precision*recall)/(precision + recall)

if VN == 0:
    especificidad = 100
else:
    especificidad = 100-100*VN/(VN+FP)

doc=load_workbook(archivo)
hoja = doc.get_sheet_by_name(doc.get_sheet_names()[0])

#print("cantidad de imagenes: " + str(cant_im))
print("Verdaderos positivos: " + str(VP))
print("Falsos positivos: " + str(FP) )
print("Falsos negativos: " + str(FN))
print("Verdaderos negativos: " + str(VN))
print("\n")
print("1-Especificidad: " + str(especificidad)+"%")
print("Recall: " + str(recall)+"%")
print("Precision: " + str(precision)+"%")
print("Exactitud: " + str(exactitud)+"%")
print("F1: " + str(f1)+"%")
print("**********************************************************************")
print("\n \n")

hoja.cell(row = 2, column = 1).value = 0.68
hoja.cell(row = 2, column = 2).value = VP
hoja.cell(row = 2, column = 3).value = VN
hoja.cell(row = 2, column = 4).value = FP
hoja.cell(row = 2, column = 5).value = FN
hoja.cell(row = 2, column = 6).value = precision
hoja.cell(row = 2, column = 7).value = exactitud
hoja.cell(row = 2, column = 8).value = recall
hoja.cell(row = 2, column = 9).value = f1
hoja.cell(row = 2, column = 10).value = especificidad
doc.save(archivo)

VP = 0
VN = 0
FP = 0
FN = 0

with open("ranking.json", "w") as f:
    json.dump(lists_k_match_path, f, ensure_ascii=False, indent=2)