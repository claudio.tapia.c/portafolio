"""
Copyright 2017-2018 Fizyr (https://fizyr.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from __future__ import print_function

import csv

import datetime

from .EmMerger import *
from object_detector_retinanet.utils import create_folder, root_dir
from .visualization import draw_detections, draw_annotations
from numba import njit, jit, prange
import numpy as np
import os

import cv2
import pandas
from ..utils.image import (
    TransformParameters,
    adjust_transform_for_image,
    apply_transform,
    preprocess_image,
    resize_image,
)

@njit
def binary_integration(kernel_dim,img,matrix_count,x1,y1,x2,y2):
    
    size = img.shape

    h = size[0]
    w = size[1]

    cant_trozos_x = int(w/kernel_dim)
    cant_trozos_y = int(h/kernel_dim)
    
    for trozo_x in prange(cant_trozos_x):
        for trozo_y in prange(cant_trozos_y):
            kernel_x1 = trozo_x*kernel_dim
            kernel_x2 = (trozo_x+1)*kernel_dim
            kernel_y1 = trozo_y*kernel_dim
            kernel_y2 = (trozo_y+1)*kernel_dim

            if (kernel_x1>=x1) and (kernel_x2<=x2) and (kernel_y1>=y1) and (kernel_y2<=y2):
                matrix_count[trozo_y, trozo_x] += 1

    return matrix_count

@njit
def binary_integration_pixel(matrix_count,x1,y1,x2,y2):
    
    size = matrix_count.shape

    h = size[0]
    w = size[1]
    
    for x in prange(w):
        for y in prange(h):
            if (x>=x1) and (x<=x2) and (y>=y1) and (y<=y2):
                matrix_count[y, x] += 1

    return matrix_count

@njit
def binary_integration_pixel(matrix_count,x1,y1,x2,y2):
    
    size = matrix_count.shape

    h = size[0]
    w = size[1]
    
    for x in prange(w):
        for y in prange(h):
            if (x>=x1) and (x<=x2) and (y>=y1) and (y<=y2):
                matrix_count[y, x] += 1

    return matrix_count


def binary_integration_per_product(list_dect, len_list):
    i = 0
    
    XX1 = []
    YY1 = []
    XX2 = []
    YY2 = []

    for detc in list_dect:

        min_x1 = min(detc['x1'])
        min_y1 = min(detc['y1'])
        max_x2 = max(detc['x2'])
        max_y2 = max(detc['y2'])
        len_x1 = len(detc['x1'])

        matrix_count = np.zeros((max_y2,max_x2))
        matrix_true = np.zeros((max_y2,max_x2))

        X1 = detc['x1']
        Y1 = detc['y1']
        X2 = detc['x2']
        Y2 = detc['y2']
        


        for index in prange(len_x1):
            x1 = X1[index]
            y1 = Y1[index]
            x2 = X2[index]
            y2 = Y2[index]
            for pixel_x in prange(x1,x2):
                for pixel_y in prange(y1,y2):
                    if (pixel_x>=x1) and (pixel_x<=x2) and (pixel_y>=y1) and (pixel_y<=y2):

                        matrix_count[pixel_y, pixel_x] += 1
                        if matrix_count[pixel_y, pixel_x]>10:
                            matrix_true[pixel_y, pixel_x] = 1
                           
        if matrix_true.max() == 1:
            for pixel_x in prange(min_x1,max_x2):
                break_1=False
                for pixel_y in prange(min_y1,max_y2):
                    if  matrix_true[pixel_y, pixel_x] == 1:
                        x1 = pixel_x
                        y1 = pixel_y
                        break_1 = True
                        break
                if break_1==True:
                    break
            
            for pixel_x in prange(max_x2,min_x1):
                break_2=False
                for pixel_y in prange(max_y2,min_y1):
                    if  matrix_true[pixel_y, pixel_x] == 1:
                        x2 = pixel_x
                        y2 = pixel_y
                        break_2 = True
                        break
                if break_2==True:
                    break
        
        else:
            x1 = None
            y1 = None
            x2 = None
            y2 = None

            return x1, y1, x2, y2
            
    return x1, y1, x2, y2
    
@njit
def binary_integration_per_product_2(X1,Y1,X2,Y2,matrix_count,matrix_true,min_x1,min_y1,max_x2,max_y2,len_x1):
   
        
    XX1 = []
    YY1 = []
    XX2 = []
    YY2 = []

    for index in prange(len_x1):
        x1 = X1[index]
        y1 = Y1[index]
        x2 = X2[index]
        y2 = Y2[index]
        for pixel_x in prange(x1,x2):
            for pixel_y in prange(y1,y2):
                if (pixel_x>=x1) and (pixel_x<=x2) and (pixel_y>=y1) and (pixel_y<=y2):

                    matrix_count[pixel_y, pixel_x] += 1
                    if matrix_count[pixel_y, pixel_x]>=19:
                        matrix_true[pixel_y, pixel_x] = 1
                        
    if matrix_true.max() == 1:
        for pixel_x in prange(min_x1,max_x2):
            break_1=False
            for pixel_y in prange(min_y1,max_y2):
                if  matrix_true[pixel_y, pixel_x] == 1:
                    x1 = pixel_x
                    y1 = pixel_y
                    break_1 = True
                    break
            if break_1==True:
                break
        
        for pixel_x in prange(max_x2,min_x1):
            break_2=False
            for pixel_y in prange(max_y2,min_y1):
                if  matrix_true[pixel_y, pixel_x] == 1:
                    x2 = pixel_x
                    y2 = pixel_y
                    break_2 = True
                    break
            if break_2==True:
                break
                        
        XX1.append(x1)
        YY1.append(y1)
        XX2.append(x2)
        YY2.append(y2)
        

    return XX1,YY1,XX2,YY2

def detections_per_product(XX1,YY1,XX2,YY2):
    det_size = len(XX1)
    alpha = -0.5
    
    XX1 = numpy.array(XX1)
    YY1 = numpy.array(YY1)
    XX2 = numpy.array(XX2)
    YY2 = numpy.array(YY2)

    XX1aM = numpy.array(XX1)*alpha
    YY1aM = numpy.array(YY1)*alpha
    XX2aM = numpy.array(XX2)*alpha
    YY2aM = numpy.array(YY2)*alpha

    XX1aD = numpy.array(XX1)//alpha
    YY1aD = numpy.array(YY1)//alpha
    XX2aD = numpy.array(XX2)//alpha
    YY2aD = numpy.array(YY2)//alpha

    len_list = 0
    list_dect = []
    idxs =[]
    for j in prange(det_size):
        idxs.append(j)

    idxs = numpy.array(idxs)
    i = 0
    while len(idxs) > 0:
        last = len(idxs) - 1
        
        i = idxs[last]

        if XX1.any():
            x1 = XX1[i]
            y1 = YY1[i]
            x2 = XX2[i]
            y2 = YY2[i]

        delete_idxs = []
        delete_idxs_n = []
        for h, j in np.ndenumerate(idxs):

            punto_medio_x = int((XX1[j]+XX2[j])//2)
            

            punto_medio_y = int((YY1[j]+YY2[j])//2)
            ancho_x_1 = XX2[j] - XX1[j]
            ancho_y_1 = YY2[j] - YY1[j]

            gamma_x_1= ancho_x_1*alpha
            gamma_y_1= ancho_y_1*alpha*2

            x_medio = int((x1+x2)/2)
            y_medio = int((y1+y2)/2)

            ancho_x_2 = x2 - x1
            ancho_y_2 = y2 - y1

            gamma_x_2= ancho_x_2*alpha
            gamma_y_2= ancho_y_2*alpha*2

            if (((punto_medio_x>=x1+gamma_x_2) and (punto_medio_y>=y1+gamma_y_2) and (punto_medio_x<=x2-gamma_x_2) and (punto_medio_y<=y2-gamma_y_2)) and ((XX1[j]+gamma_x_1<=x_medio) and (YY1[j]-gamma_y_1<=y_medio) and (XX2[j]+gamma_x_1>=x_medio) and (YY2[j]-gamma_y_1>=y_medio))):
                delete_idxs.append(j)
            else:
                delete_idxs_n.append(j)
        
        if len(delete_idxs)!=0:
            
            X1_p = XX1[delete_idxs]
            Y1_p = YY1[delete_idxs]
            X2_p = XX2[delete_idxs]
            Y2_p = YY2[delete_idxs]

            idxs = np.delete(delete_idxs_n, np.where(delete_idxs_n == last))  

            data=dict(
                    x1=X1_p.tolist(),
                    y1=Y1_p.tolist(),
                    x2=X2_p.tolist(),
                    y2=Y2_p.tolist(),
                )
            list_dect.append(data)
            
        else:
            idxs = np.delete(idxs,[last])

    
    return list_dect, len([i for i in list_dect if i])
  


def detections_per_product_2(XX1,YY1,XX2,YY2):
    det_size = len(XX1)
    alpha = 0.8
    beta = 0.4
    XX1 = numpy.array(XX1)
    YY1 = numpy.array(YY1)
    XX2 = numpy.array(XX2)
    YY2 = numpy.array(YY2)

    list_dect = []
    idxs =[]
    for j in prange(det_size):
        idxs.append(j)

    idxs = numpy.array(idxs)
    i = 0
    while len(idxs) > 0:
        last = len(idxs) - 1
        
        i = idxs[last]

        if XX1.any():
            x1 = XX1[i]
            y1 = YY1[i]
            x2 = XX2[i]
            y2 = YY2[i]

        delete_idxs = []
        delete_idxs_n = []
        for h, j in np.ndenumerate(idxs):

            #punto_medio_x = int((XX1[j]+XX2[j])//2)
            #punto_medio_y = int((YY1[j]+YY2[j])//2)

            ancho_x_1 = XX2[j] - XX1[j]
            ancho_y_1 = YY2[j] - YY1[j]

            gamma_x_1= ancho_x_1*alpha
            gamma_y_1= ancho_y_1*alpha

            omega_x_1 = ancho_x_1*beta
            omega_y_1 = ancho_y_1*beta

            #x_medio = int((x1+x2)/2)
            #y_medio = int((y1+y2)/2)

            ancho_x_2 = x2 - x1
            ancho_y_2 = y2 - y1

            gamma_x_2= ancho_x_2*alpha
            gamma_y_2= ancho_y_2*alpha

            omega_x_2 = ancho_x_2*beta
            omega_y_2 = ancho_y_2*beta

            # si XX1[j],YY1[j],XX2[j],YY2[j] está dentro del bounding box x1,y1,x2,y2
            if (((XX1[j]+gamma_x_1)>=(x1+omega_x_2)) and ((YY1[j]+gamma_y_1)>=(y1+omega_y_2)) and ((XX2[j]-gamma_x_1)<=(x2-omega_x_2)) and ((YY2[j]-gamma_y_1)<=(y2-omega_y_2))):
                delete_idxs.append(j)
            
            # si x1,y1,x2,y2 está dentro del bounding box XX1[j],YY1[j],XX2[j],YY2[j]
            elif (((XX1[j]+omega_x_1)<=(x1+gamma_x_2)) and ((YY1[j]+omega_y_1)<=(y1+gamma_y_2)) and ((XX2[j]-omega_x_1)>=(x2-gamma_x_2)) and ((YY2[j]-omega_y_1)>=(y2-gamma_y_2))):
                delete_idxs.append(j)
            else:
                delete_idxs_n.append(j)
        
        if len(delete_idxs)!=0:
            
            X1_p = XX1[delete_idxs]
            Y1_p = YY1[delete_idxs]
            X2_p = XX2[delete_idxs]
            Y2_p = YY2[delete_idxs]

            idxs = np.delete(delete_idxs_n, np.where(delete_idxs_n == last))  

            data=dict(
                    x1=X1_p.tolist(),
                    y1=Y1_p.tolist(),
                    x2=X2_p.tolist(),
                    y2=Y2_p.tolist(),
                )
            list_dect.append(data)
            
        else:
            idxs = np.delete(idxs,[last])

    
    return list_dect, len([i for i in list_dect if i])
def non_max_suppression_fast(boxes, overlapThresh):
   # if there are no boxes, return an empty list
   if len(boxes) == 0:
      return []

   # if the bounding boxes integers, convert them to floats --
   # this is important since we'll be doing a bunch of divisions
   if boxes.dtype.kind == "i":
      boxes = boxes.astype("float")
#  
   # initialize the list of picked indexes   
   pick = []

   # grab the coordinates of the bounding boxes
   x1 = boxes[:,0]
   y1 = boxes[:,1]
   x2 = boxes[:,2]
   y2 = boxes[:,3]

   # compute the area of the bounding boxes and sort the bounding
   # boxes by the bottom-right y-coordinate of the bounding box
   area = (x2 - x1 + 1) * (y2 - y1 + 1)
   idxs = np.argsort(y2)

   # keep looping while some indexes still remain in the indexes
   # list
   while len(idxs) > 0:
      # grab the last index in the indexes list and add the
      # index value to the list of picked indexes
      last = len(idxs) - 1
      i = idxs[last]
      pick.append(i)

      # find the largest (x, y) coordinates for the start of
      # the bounding box and the smallest (x, y) coordinates
      # for the end of the bounding box
      xx1 = np.maximum(x1[i], x1[idxs[:last]])
      yy1 = np.maximum(y1[i], y1[idxs[:last]])
      xx2 = np.minimum(x2[i], x2[idxs[:last]])
      yy2 = np.minimum(y2[i], y2[idxs[:last]])

      # compute the width and height of the bounding box
      w = np.maximum(0, xx2 - xx1 + 1)
      h = np.maximum(0, yy2 - yy1 + 1)

      # compute the ratio of overlap
      overlap = (w * h) / area[idxs[:last]]

      # delete all indexes from the index list that have
      idxs = np.delete(idxs, np.concatenate(([last],
         np.where(overlap > overlapThresh)[0])))

   # return only the bounding boxes that were picked using the
   # integer data type
   return boxes[pick].astype("int")


def deteccion_producto(
        model,
        path_img):

    kernel_dim = 2
    
    max_detections=9999
    
    hard_score_rate = 0
    score_threshold = 0
    
    image = cv2.imread(path_img)
    img = image.copy()
    size = img.shape

    h = size[0]
    w = size[1]

    cant_trozos_x = int(w/kernel_dim)
    cant_trozos_y = int(h/kernel_dim)
    matrix_count = np.zeros((cant_trozos_y,cant_trozos_x))

    y_max = np.arange(1,h+1)
    x_max = np.arange(1,w+1)
    image_r = image[:,:,0]
   
    print(matrix_count.shape)

    #image = preprocess_image(img.copy())
    image, scale = resize_image(image)

    # run network
    boxes, hard_scores, labels, soft_scores = model.predict_on_batch(np.expand_dims(image, axis=0))
    soft_scores = np.squeeze(soft_scores, axis=-1)
    soft_scores = hard_score_rate * hard_scores + (1 - hard_score_rate) * soft_scores
    # correct boxes for image scale
    boxes /= scale

    # select indices which have a score above the threshold
    indices = np.where(hard_scores[0, :] > score_threshold)[0]

    # select those scores
    scores = soft_scores[0][indices]
    hard_scores = hard_scores[0][indices]

    # find the order with which to sort the scores
    scores_sort = np.argsort(-scores)[:max_detections]

    # select detections
    image_boxes = boxes[0, indices[scores_sort], :]
    image_scores = scores[scores_sort]
    image_hard_scores = hard_scores[scores_sort]
    image_labels = labels[0, indices[scores_sort]]

    image_detections = np.concatenate(
        [image_boxes, np.expand_dims(image_scores, axis=1), np.expand_dims(image_labels, axis=1)], axis=1)
    results = np.concatenate(
        [image_boxes, np.expand_dims(image_scores, axis=1), np.expand_dims(image_hard_scores, axis=1),
            np.expand_dims(image_labels, axis=1)], axis=1)

    result_df = pandas.DataFrame()
    result_BIG = pandas.DataFrame()
    result_df['x1'] = results[:, 0].astype(int)
    result_df['y1'] = results[:, 1].astype(int)
    result_df['x2'] = results[:, 2].astype(int)
    result_df['y2'] = results[:, 3].astype(int)

    XX1 = results[:, 0].astype(int)
    YY1 = results[:, 1].astype(int)
    XX2 = results[:, 2].astype(int)
    YY2 = results[:, 3].astype(int)

    X1 = []
    Y1 = []
    X2 = []
    Y2 = []

    list_dect, len_list= detections_per_product_2(XX1,YY1,XX2,YY2)
    
    #detc= binary_integration_per_product(list_dect, len_list)
    
    
    X1_ = []
    Y1_ = []
    X2_ = []
    Y2_ = []

    for detc in list_dect:

        min_x1 = min(detc['x1'])
        min_y1 = min(detc['y1'])
        max_x2 = max(detc['x2'])
        max_y2 = max(detc['y2'])
        len_x1 = len(detc['x1'])

        matrix_count = np.zeros((max_y2,max_x2))
        matrix_true = np.zeros((max_y2,max_x2))

        X1 = detc['x1']
        Y1 = detc['y1']
        X2 = detc['x2']
        Y2 = detc['y2']

        x1, y1, x2, y2 = binary_integration_per_product_2(X1,Y1,X2,Y2,matrix_count,matrix_true,min_x1,min_y1,max_x2,max_y2,len_x1)

        if x1:
            X1_.append(x1[0])
            Y1_.append(y1[0])
            X2_.append(x2[0])
            Y2_.append(y2[0])
         

    # for x1,y1,x2,y2 in zip(result_df['x1'], result_df['y1'], result_df['x2'], result_df['y2']):
    #     matrix_count = binary_integration(kernel_dim,img,matrix_count,x1,y1,x2,y2)
        

    # for trozo_x in range(cant_trozos_x):
    #     for trozo_y in range(cant_trozos_y):
    #         if matrix_count[trozo_y, trozo_x] > 15:
    #             kernel_x1 = trozo_x*kernel_dim
    #             kernel_x2 = (trozo_x+1)*kernel_dim
    #             kernel_y1 = trozo_y*kernel_dim
    #             kernel_y2 = (trozo_y+1)*kernel_dim

    #             X1.append(kernel_x1)
    #             Y1.append(kernel_y1)
    #             X2.append(kernel_x2)
    #             Y2.append(kernel_y2)
                

    result_BIG['x1'] = X1_
    result_BIG['y1'] = Y1_
    result_BIG['x2'] = X2_
    result_BIG['y2'] = Y2_

    # result_BIG['x1'] = list_dect[4]['x1']
    # result_BIG['y1'] = list_dect[4]['y1'] 
    # result_BIG['x2'] = list_dect[4]['x2']
    # result_BIG['y2'] = list_dect[4]['y2'] 
    #print(result_BIG['x1'] )
    #image_detections = image_detections.to_dict()
 
    #image_detections = non_max_suppression_fast(image_detections, 0*0.1618033988749)
    
    return result_BIG