#include "deteccion_sacos.h"


c10::IValue get_caffe2_tracing_inputs(Mat& img, c10::Device device) 
{
  const int height = img.rows;
  const int width = img.cols;
  // FPN models require divisibility of 32.
  // Tracing mode does padding inside the graph, but caffe2_tracing does not.
  assert(height % 32 == 0 && width % 32 == 0);
  const int channels = 3;

  auto input =
      torch::from_blob(img.data, {1, height, width, channels}, torch::kUInt8);
  // NHWC to NCHW
  input = input.to(device, torch::kFloat).permute({0, 3, 1, 2}).contiguous();

  array<float, 3> im_info_data{height * 1.0f, width * 1.0f, 1.0f};
  auto im_info =
      torch::from_blob(im_info_data.data(), {1, 3}).clone().to(device);
  return make_tuple(input, im_info);
}

c10::IValue get_tracing_inputs(cv::Mat& img, c10::Device device) 
{
  const int height = img.rows;
  const int width = img.cols;
  const int channels = 3;

  auto input =
      torch::from_blob(img.data, {height, width, channels}, torch::kUInt8);
  // HWC to CHW
  input = input.to(device, torch::kFloat).permute({2, 0, 1}).contiguous();
  return input;
}

// create a Tuple[Dict[str, Tensor]] which is the input type of scripted model
c10::IValue get_scripting_inputs(cv::Mat& img, c10::Device device) 
{
  const int height = img.rows;
  const int width = img.cols;
  const int channels = 3;

  auto img_tensor =
      torch::from_blob(img.data, {height, width, channels}, torch::kUInt8);
  // HWC to CHW
  img_tensor =
      img_tensor.to(device, torch::kFloat).permute({2, 0, 1}).contiguous();
  auto dic = c10::Dict<string, torch::Tensor>();
  dic.insert("image", img_tensor);
  return make_tuple(dic);
}

c10::IValue get_inputs(string export_method, cv::Mat& img, c10::Device device) 
{
  // Given an image, create inputs in the format required by the model.
  if (export_method == "tracing")
    return get_tracing_inputs(img, device);
  if (export_method == "caffe2_tracing")
    return get_caffe2_tracing_inputs(img, device);
  if (export_method == "scripting")
    return get_scripting_inputs(img, device);
  abort();
}

MaskRCNNOutputs get_outputs(string export_method, c10::IValue outputs) 
{
  // Given outputs of the model, extract tensors from it to turn into a
  // common MaskRCNNOutputs format.
  if (export_method == "tracing") 
  {
    auto out_tuple = outputs.toTuple()->elements();
    // They are ordered alphabetically by their field name in Instances
    return MaskRCNNOutputs{
        out_tuple[0].toTensor(),
        out_tuple[1].toTensor(),
        out_tuple[2].toTensor(),
        out_tuple[3].toTensor()};
  }
  if (export_method == "caffe2_tracing") 
  {
    auto out_tuple = outputs.toTuple()->elements();
    // A legacy order used by caffe2 models
    return MaskRCNNOutputs{
        out_tuple[0].toTensor(),
        out_tuple[2].toTensor(),
        out_tuple[3].toTensor(),
        out_tuple[1].toTensor()};
  }
  if (export_method == "scripting") 
  {
    // With the ScriptableAdapter defined in export_model.py, the output is
    // List[Dict[str, Any]].
    auto out_dict = outputs.toList().get(0).toGenericDict();
    return MaskRCNNOutputs{
        out_dict.at("pred_boxes").toTensor(),
        out_dict.at("pred_classes").toTensor(),
        out_dict.at("pred_masks").toTensor(),
        out_dict.at("scores").toTensor()};
  }
  abort();
}