#! /usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import sys

import keras
import tensorflow as tf
import cv2

import pickle

from object_detector_retinanet.keras_retinanet import models
from object_detector_retinanet.keras_retinanet.preprocessing.csv_generator import CSVGenerator
from object_detector_retinanet.keras_retinanet.utils.predict_iou import deteccion_producto
from object_detector_retinanet.keras_retinanet.utils.keras_version import check_keras_version
from object_detector_retinanet.utils import image_path, annotation_path, root_dir
from object_detector_retinanet.keras_retinanet.matching import matching_lpips

def get_session():
    """ Construct a modified tf session.
    """
    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True
    return tf.Session(config=config)


def main(args=None):
    # parse arguments
    model_path = "/home/claudio/Documentos/SKU110K/snapshots/iou_resnet50_csv_06.h5"
    path_imagenes ="./Imagenes para etiquetar/"
    path_etiquetas = "./auto-etiquetas/"

    lista_imagenes = os.listdir(path_imagenes)

    use_cpu = False

    gpu_num = str(0)

    if use_cpu:
        os.environ["CUDA_VISIBLE_DEVICES"] = str(666)
    else:
        os.environ["CUDA_VISIBLE_DEVICES"] = gpu_num
    tf.compat.v1.keras.backend.get_session()

    # load the model
    model = models.load_model(os.path.join(root_dir(), model_path), backbone_name='resnet50', convert=1, nms=False)
    
    i = 0
    
    for path_imagen in lista_imagenes:

        i+=1
        path_imagen_split = path_imagen.split(".jpg")[0]
        f = open(path_etiquetas + path_imagen_split + ".txt",'w')
        img = cv2.imread(path_imagenes + path_imagen)
        cv2.imwrite("xxx.jpg",img)
        img_copy=img.copy()
        
        size = img_copy.shape
    
        h = size[0]
        w = size[1]
        
        # start prediction
        image_detections = deteccion_producto(model,"xxx.jpg")

        lista_bbox = []
        e = 0
        
        for x1,y1,x2,y2 in zip(image_detections['x1'], image_detections['y1'], image_detections['x2'], image_detections['y2']):

            if e == 0:
                lista_bbox.append(str(0) + " " + str("{0:.6f}".format((x1+x2)/(2*w))) + " " + str("{0:.6f}".format((y1+y2)/(2*h))) + " " + str("{0:.6f}".format((x2-x1)/w)) + " " + str("{0:.6f}".format((y2-y1)/h)))
            else:
                lista_bbox.append("\r\n" + str(0) + " " + str("{0:.6f}".format((x1+x2)/(2*w))) + " " + str("{0:.6f}".format((y1+y2)/(2*h))) + " " + str("{0:.6f}".format((x2-x1)/w)) + " " + str("{0:.6f}".format((y2-y1)/h)))

            e+=1

        for box in lista_bbox:
            f.write(box)
            
        fc = open(path_etiquetas + "classes.txt",'w')
        fc.write("other")
        f.close()
        fc.close()

if __name__ == '__main__':
    main()
