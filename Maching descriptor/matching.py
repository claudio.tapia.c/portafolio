import numpy as np
import cv2
import matplotlib.pyplot as plt
import time
import os
import imghdr

flag_match = False
flag_result = False

distancia_min = 100000000
k_max = 0
MIN_MATCH_COUNT = 8
FLANN_INDEX_KDTREE = 1

path_imagenes='/home/claudio/Escritorio/Matching descriptor/imagenes_planograma'
lista_imagenes = os.listdir(path_imagenes)

path_imagenes_test = '/home/claudio/Escritorio/Matching descriptor/imagenes_test'
lista_imagenes_test = os.listdir(path_imagenes_test)

# FLANN parameters
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 1)
search_params = dict(checks=1)   # or pass empty dictionary
flann = cv2.FlannBasedMatcher(index_params,search_params)
sift = cv2.xfeatures2d.SIFT_create()

for path_image_test in lista_imagenes_test:

    img1 = cv2.imread(path_imagenes_test + "/" + path_image_test) 
    tic = time.time()
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2RGB)
    kp1, des1 = sift.detectAndCompute(img1,None)

    for path_image in lista_imagenes:

        img2 = cv2.imread(path_imagenes + "/" + path_image) #trainImage
        img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
        # find the keypoints and descriptors with SIFT
        dense = cv2.TYPE_create()
        kp2=dense.detect(img2)
        kp2, des2 = sift.detectAndCompute(img2,None)
        matches = flann.knnMatch(des1,des2,k=2)

        good = []
        dst_pts = []
        src_pts = []
        
        # ratio test as per Lowe's paper
        for i,(m,n) in enumerate(matches):
            if m.distance < 0.8*n.distance:
                good.append(m)
                dst_pts.append(kp1[m.queryIdx].pt)
                src_pts.append(kp2[m.trainIdx].pt)

        if len(good)>MIN_MATCH_COUNT:
            dst_pts= np.float32(dst_pts).reshape(-1,1,2)
            src_pts= np.float32(src_pts).reshape(-1,1,2)
            M, mask = cv2.findHomography(src_pts, dst_pts, cv2.RANSAC,5.0)
            matchesMask_ = mask.ravel().tolist()
            k=0
            for e in matchesMask_:
                if e==1:
                    k=k+1

            #h,w,d = img1.shape
            #pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
            
            # if M is not None:
            #     _ , H, status = M
            #     dst = cv2.perspectiveTransform(pts,M)
            #     #img1 = cv2.polylines(img1,[np.int32(dst)],True,255,3, cv2.LINE_AA)

            if k_max<k:
                k_max=k
                image_match=img2 
                kp2_match=kp2
                des2_match=des2
                matches_m=good
                matchesMask_m=matchesMask_
                path_image_m = path_image     
            
        else:
            print( "\n Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT))
            matchesMask_ = None
    
    

    tac=time.time()
    draw_params = dict(matchColor = (0,255,0),
                    singlePointColor = (255,0,0),
                    matchesMask = matchesMask_m,
                    flags = cv2.DrawMatchesFlags_DEFAULT)

    tiempo_transcurrido=tac-tic
    print( "\n imagen test: {}\n imagen match: {}\n matchs: {}\n".format(path_image_test, path_image_m, k_max))
    print('tiempo transcurrido: ', tiempo_transcurrido)
    img3 = cv2.drawMatches(img1,kp1,image_match,kp2_match,matches_m,None,**draw_params)
    k_max = 0
    plt.imshow(img3,)
