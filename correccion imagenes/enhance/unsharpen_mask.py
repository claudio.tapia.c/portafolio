import cv2
from skimage import data
from skimage.filters import unsharp_mask
from skimage import img_as_ubyte
import matplotlib.pyplot as plt
from skimage import io
import os
import numpy as np


ruta_imagenes = './set_pruebas/'
ruta_salida = './sharpening_skimage/'

path_images = os.listdir(ruta_imagenes)


for path_image in path_images:
    print(ruta_imagenes + path_image)
    image = cv2.imread(ruta_imagenes + path_image)
    result = unsharp_mask(image, radius=5, amount=1)
    result = img_as_ubyte(result)
    cv2.imwrite(ruta_salida + path_image, result)
