#include "deteccion_sacos.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core/cuda.hpp>
#include "opencv2/cudaobjdetect.hpp"
#include "opencv2/cudaimgproc.hpp"
#include "opencv2/cudawarping.hpp"
#include <opencv2/cudaarithm.hpp>
#include <iostream>
#include <string>
#include <unistd.h>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

#include <c10/cuda/CUDAStream.h>
#include <torch/csrc/autograd/grad_mode.h>
#include <torch/csrc/jit/runtime/graph_executor.h>
#include <torch/script.h>

#include <cstdlib>

#define HEIGHT_IMAGE 448
#define WIDTH_IMAGE 224

using namespace cv;
using namespace std;

RNG rng(12345);
bool flagDetection = false;
bool cameraIp = false;

void foo()
{ 
  system("/usr/bin/python3 usbrelay.py --relay=2 --state=1");
  sleep(0.5);
  system("/usr/bin/python3 usbrelay.py --relay=2 --state=0");


  flagDetection = false;
}

void drawContourandBox(Mat& frame,MaskRCNNOutputs& rcnn_outputs,float frame_width, float frame_height){

  int x1 = rcnn_outputs.pred_boxes[0][0].item<float>()*(frame_width/WIDTH_IMAGE);
  int y1 = rcnn_outputs.pred_boxes[0][1].item<float>()*(frame_height/HEIGHT_IMAGE);
  int x2 = rcnn_outputs.pred_boxes[0][2].item<float>()*(frame_width/WIDTH_IMAGE);
  int y2 = rcnn_outputs.pred_boxes[0][3].item<float>()*(frame_height/HEIGHT_IMAGE);

  Mat mask_arr = Mat::zeros(rcnn_outputs.pred_masks[0][0].sizes()[0],rcnn_outputs.pred_masks[0][0].sizes()[1],CV_32FC1);

  for(int j=0;j<mask_arr.rows;j++)
  {
    for(int k=0;k<mask_arr.cols;k++)
    {
        mask_arr.at<float>(j,k) = rcnn_outputs.pred_masks[0][0][j][k].item<float>();
    }
  }
  
  Scalar color = Scalar(rng.uniform(0, 256), rng.uniform(0,256), rng.uniform(0,256));
  Rect box = Rect(x1,y1,x2-x1,y2-y1);
  Mat mask_out;
  cuda::GpuMat maskInGpu;
  cuda::GpuMat maskOutGpu;
  maskInGpu.upload(mask_arr);
  cuda::resize(maskInGpu, maskOutGpu, Size(box.width, box.height));
  cuda::threshold(maskOutGpu, maskOutGpu,0.3,1.0,THRESH_TOZERO);
  maskOutGpu.convertTo(maskOutGpu, CV_8U);
  Mat mask;
  maskOutGpu.download(mask);
  Mat coloredRoi = (0.3 * color + 0.7 * frame(box));
  cuda::GpuMat coloredRoiGpu;
  coloredRoiGpu.upload(coloredRoi);
  coloredRoiGpu.convertTo(coloredRoiGpu, CV_8UC3);
  coloredRoiGpu.download(coloredRoi);
  vector<Mat> contours;
  Mat hierarchy;
  findContours(mask, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);
  drawContours(coloredRoi, contours, -1, color, 5, LINE_8, hierarchy, 100);
  coloredRoi.copyTo(frame(box), mask);

  //cout << rcnn_outputs.pred_boxes[0][0].item<float>()*(frame_width/DIM_IMAGE) << endl;
  //cout << image_mat << endl;

  rectangle(frame, Point(x1,y1), Point(x2, y2), Scalar(0, 255, 0),2, 4);

  putText(frame,
          "Saco roto detectado", 
          Point(x1, y1),
          FONT_HERSHEY_DUPLEX,
          1.0,
          CV_RGB(128, 0, 128),
          2);
}

int main(int, char**)
{
  int frameCount = 0;

  bool DEBUG = false;

  thread th1;
  
  clock_t start, end;

  string export_method = "caffe2_tracing";

  torch::jit::script::Module module;
  
  Mat frame, frame_resize;

  cuda::GpuMat gpuInFrame;
  cuda::GpuMat gpuOutFrame;

  VideoCapture cap;
  
  string url = "http://192.168.1.106:8080/shot.jpg";
  int div = 0;   
  int deviceID = 0;

  if(cameraIp){
    cap.open(url, CAP_V4L2);}
  else{ 
    cap.open(deviceID, CAP_V4L2);}

  int codec = VideoWriter::fourcc('M','J','P','G');
  cap.set(CAP_PROP_FOURCC,codec);
  
  double cameraFrameWidth = 640.0;
  double cameraFrameHeight = 480.0;

  cap.set(CAP_PROP_FRAME_WIDTH,cameraFrameWidth);
  cap.set(CAP_PROP_FRAME_HEIGHT,cameraFrameHeight);

  cap.set(CAP_PROP_BUFFERSIZE,1);

  if (!cap.isOpened()) 
  {
      cerr << "ERROR! Unable to open camera\n";
      return -1;
  }
  
  cap.set(CAP_PROP_FPS, 90.0);

  cout << "Start grabbing" << endl
      << "Press any key to terminate" << endl;

  assert(
    export_method == "caffe2_tracing" || export_method == "tracing" ||
    export_method == "scripting");

  torch::jit::getBailoutDepth() = 1;
  torch::autograd::AutoGradMode guard(false);
  module = torch::jit::load("model.ts");
  assert(module.buffers().size() > 0);
  auto device = (*begin(module.buffers())).device();
  cout << device << endl;

  while(true)
  {
    if (true) 
      start = clock();
    
    ///cap >> frame;

    //cap.read(frame);
    cap.grab();
    cap.retrieve(frame);

    if (frame.empty()) 
    {
      cerr << "ERROR! blank frame grabbed\n";
      break;
    }

    gpuInFrame.upload(frame);
    cv::Size size = frame.size();
    cuda::rotate(gpuInFrame, gpuOutFrame,Size(size.height, size.width),-90, size.height-1, 0, INTER_LINEAR);
    gpuOutFrame.download(frame);

    float frame_width = frame.cols;
    float frame_height = frame.rows;

    gpuInFrame.upload(frame);
    cuda::resize(gpuInFrame,gpuOutFrame,Size(WIDTH_IMAGE,HEIGHT_IMAGE));
    gpuOutFrame.download(frame_resize);

    auto inputs = get_inputs(export_method, frame_resize, device);

    // Run the network
    auto output = module.forward({inputs});
    if (true) end = clock();
    
    if (device.is_cuda())
      c10::cuda::getCurrentCUDAStream().synchronize();

    auto rcnn_outputs = get_outputs(export_method, output);
  
    if ((rcnn_outputs.pred_classes.sizes()[0] != 0)&&(rcnn_outputs.pred_classes[0].item<int>() == 1)&&(rcnn_outputs.scores[0].item<float>() > 0.85)&&(!flagDetection))
    {
      drawContourandBox(frame, rcnn_outputs,frame_width,frame_height);

      flagDetection = true;
      th1 = thread(foo);
      th1.detach();
    }

    double fps = 1 / (double(end - start) / double(CLOCKS_PER_SEC));

    cout << "FPS : " << fixed << fps << setprecision(5) << endl;

    if (DEBUG)
    {
      double time_taken = double(end - start) / double(CLOCKS_PER_SEC);

      /* cout << "Number of detected objects: " << rcnn_outputs.num_instances()
          << endl;
      cout << "pred_boxes: " << rcnn_outputs.pred_boxes.toString() << " "
          << rcnn_outputs.pred_boxes.sizes() << endl;
      cout << "pred_masks: " << rcnn_outputs.pred_masks.toString() << " "
          << rcnn_outputs.pred_masks.sizes() << endl; */
      cout << "scores: " << rcnn_outputs.scores.toString() << " "
          << rcnn_outputs.scores << endl;
      cout << "pred_classes: " << rcnn_outputs.pred_classes.toString() << " "
          << rcnn_outputs.pred_classes << endl;

      cout << "Time taken by program is : " << fixed 
        << time_taken << setprecision(5);
      cout << " sec " << endl;
    }

    if (waitKey(1) >= 0)
        break;
  imshow("Deteccion sacos rotos", frame);
  }
  return 0;
}

